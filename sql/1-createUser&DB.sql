DROP SCHEMA IF EXISTS `AlienePedia` ;

CREATE SCHEMA IF NOT EXISTS `AlienePedia` DEFAULT CHARACTER SET utf8 ;
USE `AlienePedia` ;

DROP TABLE IF EXISTS `user` ;

CREATE TABLE IF NOT EXISTS `user` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(80) NULL,
  `last_name` VARCHAR(80) NULL,
  `email` VARCHAR(320) NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(80) NOT NULL,
  `is_banned` TINYINT(1) NOT NULL,
  `status` ENUM('user', 'admin') NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

DROP TABLE IF EXISTS `publication` ;

CREATE TABLE IF NOT EXISTS `publication` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `owner_id` BIGINT UNSIGNED NOT NULL,
  `date_of_publication` TIMESTAMP NOT NULL,
  `is_verified` TINYINT(1) NOT NULL,
  `name` VARCHAR(80) NOT NULL,
  `nicknames`  VARCHAR(80) NOT NULL,
  `universe` VARCHAR(80) NOT NULL,
  `origin` VARCHAR(80) NOT NULL,
  `position` ENUM('evil', 'good', 'neutrality') NOT NULL,
  `about` TEXT NULL,
  `rating` SMALLINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `ind_publication_user_id` (`owner_id` ASC),
  CONSTRAINT `fk_publication_user`
    FOREIGN KEY (`owner_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

DROP TABLE IF EXISTS `alien_photo` ;

CREATE TABLE IF NOT EXISTS `alien_photo` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `photo_url` VARCHAR(45) NOT NULL,
  `publication_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `ind_alien_photo_publication_id` (`publication_id` ASC),
  CONSTRAINT `fk_alien_photo_publication`
    FOREIGN KEY (`publication_id`)
    REFERENCES `publication` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

DROP TABLE IF EXISTS `likes` ;

CREATE TABLE IF NOT EXISTS `likes` (
  `user_id` BIGINT UNSIGNED NOT NULL,
  `publication_id` BIGINT UNSIGNED NOT NULL,
  UNIQUE (`user_id`, `publication_id`),
  INDEX `ind_user_has_publication_user_id` (`user_id` ASC),
  INDEX `ind_user_has_publication_publication_id` (`publication_id` ASC),
  CONSTRAINT `fk_user_has_publication_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_publication_publication`
    FOREIGN KEY (`publication_id`)
    REFERENCES `publication` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

DROP USER IF EXISTS `AlienePediaApp`@`localhost`;
CREATE USER IF NOT EXISTS `AlienePediaApp`@`localhost` IDENTIFIED BY 'app';
GRANT CREATE, SELECT, INSERT, UPDATE ON `AlienePedia`.* TO `AlienePediaApp`@`localhost`;
GRANT DELETE ON `AlienePedia`.`likes` TO `AlienePediaApp`@`localhost`;
FLUSH PRIVILEGES;