INSERT INTO user
VALUES (1, 'Anton', 'Sverdlov', 'sverdlov.anton@mail.ru', 'Mandragor', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', false, 'user'),
	   (2, 'Vsevolod', 'Prorashnev', 'seva.prorashnev@bk.ru', 'Ragnar', 'n0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c', false, 'user'),
       (3, null, null, null, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', false, 'admin')
ON duplicate key update first_name = VALUES(first_name), last_name = VALUES(last_name),
email = VALUES(email), username = VALUES(username), password = VALUES(password),
is_banned = VALUES(is_banned), status = VALUES(status);
INSERT INTO publication
VALUES (1, 1, '2018-12-26 13:54', true, 'Venom', 'none', 'Earth-616', 'Klintar', 'neutrality',  
'The Klyntar (colloquial: Symbiotes) are a fictional species of organic, amorphous, multicellular, extraterrestrial symbiotes appearing in American comic books published by Marvel Comics. The Klyntar bond with their hosts, creating a symbiotic bond through which a single entity is created. They also are able to slightly alter their hosts personalities, by influencing their darkest desires and wants, along with amplifying their physical and emotional traits and personality, granting them super-human abilities.
 The first appearance(s) of a symbiote occurs in The Amazing Spider-Man #252, The Spectacular Spider-Man #90, and Marvel Team-Up #141 (released concurrently in May 1984), in which Spider-Man brings one home to Earth after the Secret Wars (Secret Wars #8, which was released months later, details his first encounter with it). The concept was created by a Marvel Comics reader, with the publisher purchasing the idea for $220. The original design was then modified by Mike Zeck, becoming the Venom symbiote. The concept would be explored and used throughout multiple storylines, spin-off comics, and derivative projects.', 0),
	   (2, 2, '2018-12-27 15:23', true, 'Thanos', 'dark Lord', 'Earth-199999', 'Titan', 'evil', 'Thanos is a fictional supervillain appearing in American comic books published by Marvel Comics. The character, created by writer/artist Jim Starlin, first appeared in The Invincible Iron Man #55 (cover dated February 1973). Thanos is one of the most powerful villains in the Marvel Universe and has clashed with many heroes including the Avengers, the Guardians of the Galaxy, the Fantastic Four, and the X-Men.
The character appears in the Marvel Cinematic Universe, portrayed by Damion Poitier in The Avengers (2012), and by Josh Brolin in Guardians of the Galaxy (2014), Avengers: Age of Ultron (2015), Avengers: Infinity War (2018), and Avengers: Endgame (2019) through voice and motion capture. The character has also appeared in various comic adaptations, including animated television series, arcade, and video games. ', 0),
       (3, 1, '2018-12-27 17:56', true, 'Heimdall', 'Gatekeeper, Defender Of The Rainbow Bridge', 'Earth-199999', 'Asgard', 'good',  'Асгардцы (англ. Asgardians) - жители Асгарда. Это раса человекоподобных существ, обладающих высокоразвитыми технологиями, напоминающими магию и волшебство, на которых основана вся их цивилизация. Они охраняют мир в Девяти мирах Иггдрасиля.
Асгардцы - раса смелых и могучих воинов, чья страсть к приключениям переросла в религию, а их репутация как одних из самых могущественных рас во вселенной служит поводом для страха и уважения у других рас.
Скорее всего, фамилии асгардцев - это образование от имени родителя и его связь с потомком. Например, Тор известен как Тор Одинсон (сын Одина); он, вместе с другими асгардцами, ошибочно полагает, что то же самое принято у людей: он называл Фила Колсона сыном Кола. ', 0)
ON duplicate key update date_of_publication = VALUES(date_of_publication),
is_verified = VALUES(is_verified), name = VALUES(name), nicknames = VALUES(nicknames),
universe = VALUES(universe), origin = VALUES(origin), position = VALUES(position),
about = VALUES(about), rating = VALUES(rating);
INSERT INTO likes
VALUES (1, 3), (1, 1), (1, 2), (2, 2);
INSERT INTO alien_photo
VALUES (1, 'img/aliens/symbiot.jpg', 1), (2, 'img/aliens/titanium.jpg', 2), 
(3, 'img/aliens/au.jpg', 3) 
ON duplicate key update photo_url = VALUES(photo_url), 
publication_id=VALUES(publication_id);