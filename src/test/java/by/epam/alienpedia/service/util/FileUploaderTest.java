package by.epam.alienpedia.service.util;

import by.epam.alienpedia.exception.ServiceException;
import org.apache.commons.fileupload.FileItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FileUploaderTest {

    @Mock
    private FileItem mockFileItem;

    private static final FileUploader UPLOADER = FileUploader.getInstance();
    private static final String EXCEPTED_FILE_PATH = "img/aliens/test.png";

    @Before
    public void setUp() throws Exception {
        when(mockFileItem.getName()).thenReturn("test.png");
        doNothing().when(mockFileItem).write(any(File.class));
    }

    @Test
    public void shouldReturnFilePath() throws ServiceException {
        String actualFilePath = UPLOADER.upload(mockFileItem);
        assertEquals(EXCEPTED_FILE_PATH, actualFilePath);
    }
}
