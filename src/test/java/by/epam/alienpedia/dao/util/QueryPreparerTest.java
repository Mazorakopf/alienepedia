package by.epam.alienpedia.dao.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class QueryPreparerTest {

    @Mock
    private PreparedStatement mockPreparedStmnt;

    private long longTest;
    private int intTest;
    private String stringTest;
    private LocalDateTime dateTimeTest;
    private double doubleTest;

    public QueryPreparerTest() {
    }

    @Before
    public void setUp() throws SQLException {
        doNothing().when(mockPreparedStmnt).setString(anyInt(), anyString());
        doNothing().when(mockPreparedStmnt).setLong(anyInt(), anyLong());
        doNothing().when(mockPreparedStmnt).setInt(anyInt(), anyInt());
        doNothing().doThrow(SQLException.class).when(mockPreparedStmnt).setObject(anyInt(), any(LocalDateTime.class));

        longTest = 4L;
        intTest = 1;
        stringTest = "test";
        dateTimeTest = LocalDateTime.now();
        doubleTest = 3.0;
    }

    @Test
    public void shouldPreparerStatementWithNoException() throws SQLException {
        QueryPreparer.prepare(mockPreparedStmnt, longTest, stringTest, intTest, doubleTest, dateTimeTest);

        verify(mockPreparedStmnt, times(1)).setLong(anyInt(), anyLong());
        verify(mockPreparedStmnt, times(1)).setString(anyInt(), anyString());
        verify(mockPreparedStmnt, times(1)).setInt(anyInt(), anyInt());
        verify(mockPreparedStmnt, times(1)).setObject(anyInt(), anyDouble());
        verify(mockPreparedStmnt, times(1)).setObject(anyInt(), any(LocalDateTime.class));
    }

    @Test(expected = SQLException.class)
    public void shouldPreparerStatementWithSqlException() throws SQLException {
        QueryPreparer.prepare(mockPreparedStmnt, longTest, stringTest, intTest, doubleTest, dateTimeTest, dateTimeTest);
    }
}
