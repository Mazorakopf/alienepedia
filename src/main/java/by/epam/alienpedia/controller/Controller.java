package by.epam.alienpedia.controller;

import by.epam.alienpedia.command.Command;
import by.epam.alienpedia.command.CommandResult;
import by.epam.alienpedia.command.factory.CommandFactory;
import by.epam.alienpedia.exception.CommandException;
import lombok.extern.log4j.Log4j2;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Provides a HTTP servlet class suitable for a Web site.
 */
@Log4j2
public class Controller extends HttpServlet {

    private static final String ERROR_PAGE_PATH = "/WEB-INF/jsp/error.jsp";
    private static final String UNSUPPORTED_METHOD_EXCEPTION_MESSAGE = "Unsupported HttpServlet method.";

    /**
     * Name of the request attribute that stores the name of command.
     */
    private static final String COMMAND = "command";

    private static final String GET_METHOD = "get";
    private static final String POST_METHOD = "post";

    @Override
    protected void doGet(final HttpServletRequest request,
                         final HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(final HttpServletRequest request,
                          final HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Processes the request by obtaining a command from the
     * {@link HttpServletRequest} object, execute this command and redirects
     * or forwards on destination page depending on the result of the command.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request.
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client.
     * @throws ServletException General exception a servlet can throw when
     *                          it encounters difficulty.
     * @throws IOException      Signals that an I/O exception of some sort has occurred.
     */
    private void processRequest(final HttpServletRequest request,
                                final HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter(COMMAND);
        log.info("Command name = " + command);

        Command action = CommandFactory.create(command);

        CommandResult commandResult;
        try {
            String methodName = request.getMethod();
            if (methodName.equalsIgnoreCase(POST_METHOD)) {
                commandResult = action.executePost(request, response);
            } else if (methodName.equalsIgnoreCase(GET_METHOD)) {
                commandResult = action.executeGet(request, response);
            } else {
                throw new CommandException(UNSUPPORTED_METHOD_EXCEPTION_MESSAGE);
            }
        } catch (CommandException e) {
            log.error(e.getMessage(), e);
            commandResult = new CommandResult(ERROR_PAGE_PATH, false);
        }

        String page = commandResult.getPage();
        if (commandResult.isRedirect()) {
            sendRedirect(response, page);
        } else {
            dispatch(request, response, page);
        }
    }

    /**
     * Forwards to destination page.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request.
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client.
     * @param page     an {@link String} object that contains forwarding page.
     * @throws ServletException General exception a servlet can throw when
     *                          it encounters difficulty.
     * @throws IOException      Signals that an I/O exception of some sort has occurred.
     */
    private void dispatch(final HttpServletRequest request,
                          final HttpServletResponse response,
                          final String page) throws ServletException, IOException {
        ServletContext servletContext = getServletContext();
        RequestDispatcher requestDispatcher =
                servletContext.getRequestDispatcher(page);
        requestDispatcher.forward(request, response);
    }


    /**
     * Redirects to destination page.
     *
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client.
     * @param page     An {@link String} object that contains redirecting page.
     * @throws IOException Signals that an I/O exception of some sort has occurred.
     */
    private void sendRedirect(final HttpServletResponse response,
                              final String page) throws IOException {
        response.sendRedirect(page);
    }

}
