/**
 * This package contains the controller classes, filters and utils
 * in accordance with MVC pattern.
 */
package by.epam.alienpedia.controller;