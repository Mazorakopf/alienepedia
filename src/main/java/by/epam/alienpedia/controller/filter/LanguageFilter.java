package by.epam.alienpedia.controller.filter;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * {@code LanguageFilter} designed to perform a language filter operation.
 */
public class LanguageFilter implements Filter {

    private static final String DE = "DE";
    private static final String EN = "EN";
    /**
     * Name of the session attribute that stores the locale.
     */
    private static final String LANGUAGE = "language";
    /**
     * Name of the session attribute that stores the next locale.
     */
    private static final String NEXT_LANGUAGE = "nextLanguage";

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    /**
     * Method of the Filter is called by the container each time
     * a request/response pair is passed through the chain due to a client
     * request for a resource at the end of the chain, checks the language
     * setting and sets the default if the language has not been set.
     *
     * @param servletRequest  An {@link ServletRequest} object that
     *                        contains client request.
     * @param servletResponse An {@link ServletResponse} object
     *                        that contains the response the servlet.
     * @param chain           An {@link FilterChain} object that allows the Filter to pass
     *                        on the request and response to the next entity in the chain.
     * @throws IOException      Signals that an I/O exception of some sort has occurred.
     * @throws ServletException General exception a servlet can throw
     *                          when it encounters difficulty.
     */
    @Override
    public void doFilter(final ServletRequest servletRequest,
                         final ServletResponse servletResponse,
                         final FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession();
        String language = (String) session.getAttribute(LANGUAGE);
        if (language == null) {
            session.setAttribute(LANGUAGE, EN);
            session.setAttribute(NEXT_LANGUAGE, DE);
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() { }
}
