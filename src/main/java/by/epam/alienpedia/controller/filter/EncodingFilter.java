package by.epam.alienpedia.controller.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * {@code EncodingFilter} designed to perform a encoding filter operation.
 */
public class EncodingFilter implements Filter {

    /**
     * The name of the parameter which stores the value of the encoding
     * of the web application.
     */
    private static final String DEFAULT_INIT_ENCODING = "encoding";
    private String encoding;

    /**
     * The method initializes the value of the encoding field by extracting
     * the init parameters from {@code FilterConfig}.
     *
     * @param config An {@link FilterConfig} object that
     *               contains filter init parameters.
     * @throws ServletException General exception a servlet can throw
     *                          when it encounters difficulty.
     */
    @Override
    public void init(final FilterConfig config) throws ServletException {
        encoding = config.getInitParameter(DEFAULT_INIT_ENCODING);
    }

    /**
     * Method of the Filter is called by the container each time
     * a request/response pair is passed through the chain due to a client
     * request for a resource at the end of the chain, checks the encoding
     * setting and sets the default if the encoding has not equal UTF-8.
     *
     * @param request  An {@link ServletRequest} object that
     *                        contains client request.
     * @param response An {@link ServletResponse} object
     *                        that contains the response the servlet.
     * @param chain An {@link FilterChain} object that allows the Filter to pass
     *              on the request and response to the next entity in the chain.
     * @throws IOException Signals that an I/O exception of some sort has occurred.
     * @throws ServletException General exception a servlet can throw
     *              when it encounters difficulty.
     */
    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain chain) throws IOException, ServletException {

        String encodingRequest = request.getCharacterEncoding();
        if (encodingRequest == null || !encodingRequest.equalsIgnoreCase(encoding)) {
            request.setCharacterEncoding(encoding);
            response.setCharacterEncoding(encoding);
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        encoding = null;
    }
}
