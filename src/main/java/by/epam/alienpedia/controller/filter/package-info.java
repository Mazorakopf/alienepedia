/**
 * A package of filter classes that handle the request,
 * response between the client and the server.
 */
package by.epam.alienpedia.controller.filter;