package by.epam.alienpedia.controller.filter;

import by.epam.alienpedia.command.Command;
import by.epam.alienpedia.command.factory.CommandFactory;
import by.epam.alienpedia.model.entity.Role;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * {@code SecureFilter} designed to perform a secure filter operation.
 */
public class SecureFilter implements Filter {

    private static final String LOGIN_PAGE_PATH = "/WEB-INF/jsp/login.jsp";

    /**
     * Name of the request attribute that stores the error access message.
     */
    private static final String ERROR_ACCESS_MESSAGE = "errorAccessMessage";
    /**
     * Name of the request attribute that stores the name of command.
     */
    private static final String COMMAND = "command";
    /**
     * Name of the session attribute that stores the user's role.
     */
    private static final String ROLE = "role";

    private static final String AUTHENTICATION_MESSAGE = "You should login to view this page";
    private static final String ACCESS_USER_MESSAGE = "You should login as admin to view this page";
    private static final String ACCESS_ADMIN_MESSAGE = "You should login as user to view this page";

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    /**
     * Method of the Filter is called by the container each time
     * a request/response pair is passed through the chain due to a client
     * request for a resource at the end of the chain, checks the user role
     * and protects against unauthorized access to a page that does not match
     * the user role.
     *
     * @param servletRequest  An {@link ServletRequest} object that
     *                        contains client request.
     * @param servletResponse An {@link ServletResponse} object
     *                        that contains the response the servlet.
     * @param chain           An {@link FilterChain} object that allows the Filter to pass
     *                        on the request and response to the next entity in the chain.
     * @throws IOException      Signals that an I/O exception of some sort has
     *                          occurred.
     * @throws ServletException General exception a servlet can throw
     *                          when it encounters difficulty.
     */
    @Override
    public void doFilter(final ServletRequest servletRequest,
                         final ServletResponse servletResponse,
                         final FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        Role role = getRoleFromSession(request);

        String commandName = request.getParameter(COMMAND);
        Command command = CommandFactory.create(commandName);
        System.out.println(commandName);

        if (role.equals(Role.GUEST) && !command.isAllowedRoles(role)) {
            forwardToLoginWithMessage(request, response, AUTHENTICATION_MESSAGE);
        } else if (role.equals(Role.USER) && !command.isAllowedRoles(role)) {
            forwardToLoginWithMessage(request, response, ACCESS_USER_MESSAGE);
        } else if (role.equals(Role.ADMIN) && !command.isAllowedRoles(role)) {
            forwardToLoginWithMessage(request, response, ACCESS_ADMIN_MESSAGE);
        } else {
            chain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
    }

    /**
     * The method is designed to extract the user role from the session
     * and correct it. If the role from session equal {@code null}, then
     * the result role will be {@code Role.CUEST}, else role from session
     * equal result role.
     *
     * @param request An {@link HttpServletRequest} object that
     *                contains client request.
     * @return Correct role from enum class {@code Role}.
     */
    private Role getRoleFromSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Role roleFromSession = (Role) session.getAttribute(ROLE);
        return roleFromSession == null ? Role.GUEST : roleFromSession;
    }

    /**
     * The method designed to forward user on login page
     * with appropriate message.
     *
     * @param request  An {@link ServletRequest} object that
     *                 contains client request.
     * @param response An {@link ServletResponse} object
     *                 that contains the response the servlet.
     * @param message  About blocking access to the page with current role.
     * @throws IOException      Signals that an I/O exception of some sort has
     *                          occurred.
     * @throws ServletException General exception a servlet can throw
     *                          when it encounters difficulty.
     */
    private void forwardToLoginWithMessage(final HttpServletRequest request,
                                           final HttpServletResponse response,
                                           final String message) throws IOException, ServletException {
        request.setAttribute(ERROR_ACCESS_MESSAGE, message);
        ServletContext servletContext = request.getServletContext();
        RequestDispatcher dispatcher =
                servletContext.getRequestDispatcher(LOGIN_PAGE_PATH);
        dispatcher.forward(request, response);
    }
}
