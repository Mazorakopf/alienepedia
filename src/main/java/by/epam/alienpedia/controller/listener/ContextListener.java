package by.epam.alienpedia.controller.listener;

import by.epam.alienpedia.dao.connection.ConnectionPool;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.SQLException;

@Log4j2
public class ContextListener implements ServletContextListener {

    private ConnectionPool connectionPool;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        connectionPool = ConnectionPool.getInstance();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            connectionPool.shutDown();
        } catch (SQLException e) {
            log.error("Connection pool doesn't close.", e);
        }
    }
}
