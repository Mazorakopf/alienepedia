package by.epam.alienpedia.controller.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Utility class {@code PageManager} to retrieve the referer
 * and previous page from the request.
 */
public final class PageManager {

    /**
     * Name of the attribute that stores url of the referer page.
     */
    private static final String REFERER = "referer";
    /**
     * The command that returns to the main page of the web application.
     */
    private static final String MAIN_COMMAND = "controller?command=main";

    /**
     * @param request An {@link HttpServletRequest} object
     *                that contains client request.
     * @return The url of the previous page.
     */
    public static String getPreviousPage(final HttpServletRequest request) {
        String refPage = getRefererPage(request);
        return (refPage == null || refPage.isEmpty()) ? MAIN_COMMAND : refPage;
    }

    /**
     * @param request An {@link HttpServletRequest} object
     *                that contains client request.
     * @return The url of the referer page.
     */
    public static String getRefererPage(final HttpServletRequest request) {
        return request.getHeader(REFERER);
    }

    private PageManager() {
    }
}
