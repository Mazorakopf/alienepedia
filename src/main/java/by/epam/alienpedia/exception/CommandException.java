package by.epam.alienpedia.exception;

/**
 * Designed for signal that command exception of some sort has occurred.
 */
public class CommandException extends Exception {

    public CommandException() {
    }

    public CommandException(final String message) {
        super(message);
    }

    public CommandException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CommandException(final Throwable cause) {
        super(cause);
    }
}
