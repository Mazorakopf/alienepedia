package by.epam.alienpedia.exception;

/**
 * Designed for signal that an database access object exception of some sort has occurred.
 */
public class DaoException extends Exception {

    public DaoException() {
    }

    public DaoException(final String message) {
        super(message);
    }

    public DaoException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DaoException(final Throwable cause) {
        super(cause);
    }
}
