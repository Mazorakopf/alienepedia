/**
 * This package contains exception classes for different layers
 * of this web application.
 */
package by.epam.alienpedia.exception;