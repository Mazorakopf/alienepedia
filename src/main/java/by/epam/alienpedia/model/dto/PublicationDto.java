package by.epam.alienpedia.model.dto;

import by.epam.alienpedia.model.entity.AlienPhoto;
import by.epam.alienpedia.model.entity.Publication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublicationDto {

    private Publication publication;
    private List<AlienPhoto> photos;
    private boolean liked;

    public PublicationDto(final Publication publication,
                          final List<AlienPhoto> photos) {
        this(publication, photos, false);
    }
}
