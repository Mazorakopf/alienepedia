package by.epam.alienpedia.model.entity;

public enum Role {
    ADMIN("admin"), USER("user"), GUEST("guest");

    private String value;

    Role(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
