package by.epam.alienpedia.model.entity;

public enum Position {
    EVIL("evil"), GOOD("good"), NEUTRALITY("neutrality");

    private String value;

    Position(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
