/**
 * This package contains entity objects which belongs to model layer.
 */
package by.epam.alienpedia.model.entity;