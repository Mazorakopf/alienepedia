package by.epam.alienpedia.model.entity;

import lombok.*;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class User extends Entity {

    private long id;
    private String firstName;
    private String lastName;
    private String email;
    @NonNull
    private String userName;
    @NonNull
    private String password;
    private boolean banned;
    @NonNull
    private Role role;
}
