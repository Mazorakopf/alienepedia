package by.epam.alienpedia.model.entity;

import lombok.*;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class AlienPhoto extends Entity {

    private long id;
    private long publicationId;
    @NonNull
    private String url;

}
