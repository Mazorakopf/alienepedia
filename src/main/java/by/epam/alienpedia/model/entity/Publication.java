package by.epam.alienpedia.model.entity;

import lombok.*;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class Publication extends Entity {

    private long id;
    private long ownerId;
    @NonNull
    private LocalDateTime dateOfPublication;
    private boolean verified;
    @NonNull
    private String name;
    @NonNull
    private String nicknames;
    @NonNull
    private String universe;
    @NonNull
    private String origin;
    @NonNull
    private Position position;
    private String about;
    private int rating;
}
