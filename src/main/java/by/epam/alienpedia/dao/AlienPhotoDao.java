package by.epam.alienpedia.dao;

import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.AlienPhoto;

import java.util.List;

/**
 * Designed to determine the behavior of the implementing class in the form of
 * database access objects of type {@link AlienPhoto}.
 */
public abstract class AlienPhotoDao extends AbstractDao<AlienPhoto> {

    public abstract List<AlienPhoto> findAlienPhotosByPublicationId(long id) throws DaoException;
}
