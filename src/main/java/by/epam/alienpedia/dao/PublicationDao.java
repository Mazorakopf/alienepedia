package by.epam.alienpedia.dao;

import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.Publication;

import java.util.List;

/**
 * Designed to determine the behavior of the implementing class in the form of
 * database access objects of type {@link Publication}.
 */
public abstract class PublicationDao extends AbstractDao<Publication> {

    public abstract List<Publication> findVerifiedPublications(int position, int publicationPerPage) throws DaoException;

    public abstract List<Publication> findUserPublications(long userId) throws DaoException;

    public abstract long findNumberOfPublication() throws DaoException;

    public abstract boolean isLikedPublication(long publicationId, long userId) throws DaoException;

    public abstract long likedPublication(long publicationId, long userId) throws DaoException;

    public abstract long upPublicationRating(long publicationId) throws DaoException;

    public abstract long unLikedPublication(long publicationId, long userId) throws DaoException;

    public abstract long downPublicationRating(long publicationId) throws DaoException;
}
