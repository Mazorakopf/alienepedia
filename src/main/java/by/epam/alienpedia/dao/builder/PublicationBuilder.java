package by.epam.alienpedia.dao.builder;

import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.Position;
import by.epam.alienpedia.model.entity.Publication;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

/**
 * Designed to build an object of type {@link by.epam.alienpedia.model.entity.Publication}
 * with specified characteristics.
 */
public class PublicationBuilder implements Builder<Publication> {

    private static final String ID = "id";
    private static final String OWNER_ID = "owner_id";
    private static final String DATE_PUBLICATION = "date_of_publication";
    private static final String IS_VERIFIED = "is_verified";
    private static final String RATING = "rating";
    private static final String NAME = "name";
    private static final String NICKNAMES = "nicknames";
    private static final String UNIVERSE = "universe";
    private static final String ORIGIN = "origin";
    private static final String POSITION = "position";
    private static final String ABOUT = "about";

    /**
     * Builds an object of type Publication with properties.
     *
     * @param resultSet Instance of {@link java.sql.ResultSet} with property
     *                  set to build an object type User.
     * @return Returns built object type Publication.
     * @throws DaoException Throws when SQL Exception is caught.
     */
    @Override
    public Publication build(final ResultSet resultSet) throws DaoException {
        Publication publication;
        try {
            long id = resultSet.getLong(ID);
            long ownerId = resultSet.getLong(OWNER_ID);
            LocalDateTime date = resultSet
                    .getObject(DATE_PUBLICATION, LocalDateTime.class);
            boolean verified = resultSet.getBoolean(IS_VERIFIED);
            String name = resultSet.getString(NAME);
            String nicknames = resultSet.getString(NICKNAMES);
            String universe = resultSet.getString(UNIVERSE);
            String origin = resultSet.getString(ORIGIN);

            String stringPosition = resultSet.getString(POSITION);
            stringPosition = stringPosition.toUpperCase();
            Position position = Position.valueOf(stringPosition);

            String about = resultSet.getString(ABOUT);
            int rating = resultSet.getInt(RATING);

            publication = new Publication(id, ownerId, date, verified, name,
                    nicknames, universe, origin, position, about, rating);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        return publication;
    }
}
