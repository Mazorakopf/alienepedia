package by.epam.alienpedia.dao.builder;

/**
 * Designed to build an object of type {@link Builder}.
 */
public final class BuilderFactory {


    private static final String USER = "user";
    private static final String PUBLICATION = "publication";
    private static final String ALIEN_PHOTO = "alien_photo";

    private BuilderFactory() {
    }

    /**
     * Designed to build an object of type {@link Builder} depends on builder name.
     *
     * @param builderName A {@link String} object that contains builder name
     * @return An object of type {@link Builder}.
     */
    public static Builder create(final String builderName) {

        switch (builderName) {
            case USER:
                return new UserBuilder();
            case PUBLICATION:
                return new PublicationBuilder();
            case ALIEN_PHOTO:
                return new AlienPhotoBuilder();
            default:
                throw new IllegalArgumentException("Unknown Builder name!");
        }
    }
}
