/**
 * This package contains the Builder classes that create objects
 * based on {@link java.sql.ResultSet}
 */
package by.epam.alienpedia.dao.builder;