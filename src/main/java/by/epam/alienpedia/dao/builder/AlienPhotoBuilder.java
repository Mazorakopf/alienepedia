package by.epam.alienpedia.dao.builder;

import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.AlienPhoto;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Designed to build an object of type {@link by.epam.alienpedia.model.entity.AlienPhoto}
 * with specified characteristics.
 */
public class AlienPhotoBuilder implements Builder<AlienPhoto> {

    private static final String ID_PHOTO = "id";
    private static final String ID_PUBLICATION = "publication_id";
    private static final String URL = "photo_url";

    /**
     * Builds an object of type AlienPhoto with properties.
     *
     * @param resultSet Instance of {@link java.sql.ResultSet} with property
     *                  set to build an object type User.
     * @return Returns built object type AlienPhoto.
     * @throws DaoException Throws when SQL Exception is caught.
     */
    @Override
    public AlienPhoto build(final ResultSet resultSet) throws DaoException {
        try {
            long idPhoto = resultSet.getLong(ID_PHOTO);
            long idPublication = resultSet.getLong(ID_PUBLICATION);
            String url = resultSet.getString(URL);

            return new AlienPhoto(idPhoto, idPublication, url);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }
}
