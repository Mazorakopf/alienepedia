package by.epam.alienpedia.dao.builder;

import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.Entity;

import java.sql.ResultSet;

/**
 * Designed to determine the behavior of the implementing class in the form
 * of construction an object of type E with specified characteristics.
 *
 * @param <E> - type of object.
 */
public interface Builder<E extends Entity> {

    /**
     * Builds an object of type E with properties.
     *
     * @param resultSet Instance of {@link java.sql.ResultSet} with property set
     *                  to build an object type E.
     * @return Returns built object type E.
     * @throws DaoException Throws when SQL Exception is caught.
     */
    E build(ResultSet resultSet) throws DaoException;
}
