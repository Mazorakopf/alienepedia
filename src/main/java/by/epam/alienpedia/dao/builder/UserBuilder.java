package by.epam.alienpedia.dao.builder;

import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.Role;
import by.epam.alienpedia.model.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Designed to build an object of type {@link by.epam.alienpedia.model.entity.User}
 * with specified characteristics.
 */
public class UserBuilder implements Builder<User> {

    private static final String ID = "id";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String IS_BANNED = "is_banned";
    private static final String ROLE = "status";

    /**
     * Builds an object of type User with properties.
     *
     * @param resultSet Instance of {@link java.sql.ResultSet} with property
     *                  set to build an object type User.
     * @return Returns built object type User.
     * @throws DaoException Throws when SQL Exception is caught.
     */
    @Override
    public User build(final ResultSet resultSet) throws DaoException {
        User user;
        try {
            long id = resultSet.getLong(ID);
            String firstName = resultSet.getString(FIRST_NAME);
            String lastName = resultSet.getString(LAST_NAME);
            String email = resultSet.getString(EMAIL);
            String username = resultSet.getString(USERNAME);
            String password = resultSet.getString(PASSWORD);
            boolean isBanned = resultSet.getBoolean(IS_BANNED);

            String roleString = resultSet.getString(ROLE);
            roleString = roleString.toUpperCase();
            Role role = Role.valueOf(roleString);

            user = new User(id, firstName, lastName, email,
                    username, password, isBanned, role);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        return user;
    }
}
