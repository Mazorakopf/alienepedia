package by.epam.alienpedia.dao.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Designed for query {@link PreparedStatement} preparing.
 */
public final class QueryPreparer {

    private QueryPreparer() {
    }

    /**
     * The method preparing query from parametrized format into simple format
     * by replacing "?" sign on given parameter.
     *
     * @param preparedStatement A {@link PreparedStatement} object that contains
     *                          query that should be prepared.
     * @param params            A {@link Object} that contains parameters that should be
     *                          substituted instead of "?" sign.
     * @throws SQLException Signal that an exception that provides information
     *                      on a database has occurred.
     */
    public static void prepare(final PreparedStatement preparedStatement,
                               final Object... params) throws SQLException {
        String stringParam;
        Integer intParam;
        Long longParam;

        int length = params.length;
        for (int i = 0; i < length; i++) {
            if (params[i] instanceof String) {
                stringParam = (String) params[i];
                preparedStatement.setString(i + 1, stringParam);
            } else if (params[i] instanceof Integer) {
                intParam = (Integer) params[i];
                preparedStatement.setInt(i + 1, intParam);
            } else if (params[i] instanceof Long) {
                longParam = (Long) params[i];
                preparedStatement.setLong(i + 1, longParam);
            } else {
                preparedStatement.setObject(i + 1, params[i]);
            }
        }
    }
}
