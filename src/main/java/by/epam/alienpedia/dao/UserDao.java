package by.epam.alienpedia.dao;

import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.User;

import java.util.Optional;

/**
 * Designed to determine the behavior of the implementing class in the form of
 * database access objects of type {@link User}.
 */
public abstract class UserDao extends AbstractDao<User> {

    /**
     * Method designed for searching user depends on user login and password.
     *
     * @param login    Is a {@link String} object that contains user login
     * @param password Is a {@link String} object that contains user password
     * @return An {@link Optional} object with finding {@link User} inside.
     * @throws DaoException Signals that an database access object exception of
     *                      some sort has occurred.
     */
    public abstract Optional<User> findUserByLoginAndPassword(String login, String password) throws DaoException;

    /**
     * Method designed for searching user depends on user login.
     *
     * @param login    Is a {@link String} object that contains user login
     * @return An {@link Optional} object with finding {@link User} inside.
     * @throws DaoException Signals that an database access object exception of
     *                  some sort has occurred.
     */
    public abstract Optional<User> findUserByLogin(String login) throws DaoException;
}
