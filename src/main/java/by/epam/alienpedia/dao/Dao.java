package by.epam.alienpedia.dao;

import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.Entity;

import java.util.List;
import java.util.Optional;

/**
 * Designed to determine the behavior of the implementing class in the form
 * of database access objects of type T.
 *
 * @param <E> Type of object.
 */
public interface Dao<E extends Entity> extends AutoCloseable {

    /**
     * Performs a parameterized read query to a database to find all object type E.
     *
     * @return A {@link List} implementation with all finding objects.
     * @throws DaoException Signals that an database access object exception of
     *                      some sort has occurred.
     */
    List<E> findAll() throws DaoException;

    /**
     * Performs a parameterized read query to the database, expecting a single
     * result in the form of an object of type E with the specified identifier.
     *
     * @param id An object identifier in database.
     * @return An {@link Optional} implementation with object.
     * @throws DaoException Signals that an database access object exception of
     *              some sort has occurred.
     */
    Optional<E> findById(long id) throws DaoException;

    /**
     * The method designed for the process of creating a objects in database.
     *
     * @param entity An object type E that should be create to database.
     * @return Created identifier in database.
     * @throws DaoException Signals that an database access object exception of
     *              some sort has occurred.
     */
    long create(E entity) throws DaoException;

    /**
     * The method designed for the process of updating a objects in database.
     *
     * @param entity An object type E that should be create to database.
     * @return Created identifier in database.
     * @throws DaoException Signals that an database access object exception of
     *              some sort has occurred.
     */
    long update(E entity) throws DaoException;
}
