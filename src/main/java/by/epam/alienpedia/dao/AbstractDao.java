package by.epam.alienpedia.dao;

import by.epam.alienpedia.dao.builder.Builder;
import by.epam.alienpedia.dao.builder.BuilderFactory;
import by.epam.alienpedia.dao.connection.ConnectionPool;
import by.epam.alienpedia.dao.util.QueryPreparer;
import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.Entity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class provides a skeletal implementation of the {@link Dao} interface,
 * to minimize the effort required to implement this interface.
 */
public abstract class AbstractDao<E extends Entity> implements Dao<E> {

    private ConnectionPool connectionPool;
    private Connection connection;

    private static final String GET_ALL_QUERY = "SELECT * FROM ";
    private static final String WHERE_ID_CONDITION = " WHERE id = ?";

    public AbstractDao() {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.getConnection();
    }

    protected abstract String getTableName();

    protected Connection getConnection() {
        return connection;
    }

    /**
     * Performs a parameterized read query to a database with parameters,
     * waiting for a set of objects, and builds them with the help of a concrete
     * builder implementation.
     *
     * @param query   A {@link String} object that contains database query.
     * @param builder An implementation of {@link Builder} with a concrete class
     *                whose objects are to be built.
     * @param params  A {@link Object} that contains parameters that should be
     *                substituted in query.
     * @return A {@link List} implementation with objects.
     * @throws DaoException Signals that an database access object exception
     *                      of some sort has occurred.
     */
    protected List<E> executeQuery(final String query, final Builder<E> builder,
                                   final Object... params) throws DaoException {
        List<E> entities = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            QueryPreparer.prepare(preparedStatement, params);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                E entity = builder.build(rs);
                entities.add(entity);
            }

        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        return entities;
    }

    /**
     * Performs a parameterized read query to a database with parameters,
     * waiting for an object, and builds them with the help of a concrete
     * builder implementation.
     *
     * @param query   A {@link String} object that contains database query.
     * @param builder An implementation of {@link Builder} with a concrete class
     *                whose object is to be built.
     * @param params  A {@link Object} that contains parameters that should be
     *                substituted in query.
     * @return An {@link Optional} implementation with object.
     * @throws DaoException Signals that an database access object exception
     *                      of some sort has occurred.
     */
    protected Optional<E> executeQueryForSingleResult(final String query,
                                                      final Builder<E> builder,
                                                      final Object... params) throws DaoException {
        List<E> items = executeQuery(query, builder, params);

        return items.size() == 1
                ? Optional.of(items.get(0))
                : Optional.empty();
    }

    /**
     * Performs a parameterized update query to a database with parameters.
     *
     * @param query  A {@link String} object that contains database query.
     * @param params A {@link Object} that contains parameters that should be
     *               substituted in query.
     * @return A {@link long} identifier that was created during executing query
     * or 0 if no identifier was created.
     * @throws DaoException Signals that an database access object exception
     *                      of some sort has occurred.
     */
    protected long executeUpdate(final String query,
                                 final Object... params) throws DaoException {
        try (PreparedStatement preparedStatement = connection
                .prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

            QueryPreparer.prepare(preparedStatement, params);

            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();

            long generatedKey = 0;
            if (generatedKeys.next()) {
                generatedKey = generatedKeys.getLong(1);
            }
            return generatedKey;
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    /**
     * Performs a parameterized read query to a database with parameters,
     * and return generated values.
     *
     * @param query  A {@link String} object that contains database query.
     * @param params A {@link Object} that contains parameters that should be
     *               substituted in query.
     * @return A {@link Object} that was created during executing query
     * or {@code null} if no identifier was created.
     * @throws DaoException Signals that an database access object exception
     *                      of some sort has occurred.
     */
    protected Object getGeneratedValue(final String query,
                                       final Object... params) throws DaoException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            QueryPreparer.prepare(preparedStatement, params);

            ResultSet generatedValues = preparedStatement.executeQuery();

            Object generatedValue = null;
            if (generatedValues.next()) {
                generatedValue = generatedValues.getObject(1);
            }
            return generatedValue;
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    /**
     * Performs query to a database with parameters, and return generated values.
     *
     * @param query A {@link String} object that contains database query.
     * @return A {@link Object} that was created during executing query
     * or {@code null} if no identifier was created.
     * @throws DaoException Signals that an database access object exception
     *                      of some sort has occurred.
     */
    protected Object getGeneratedValue(final String query) throws DaoException {
        try (Statement statement = connection.createStatement()) {

            ResultSet generatedValues = statement.executeQuery(query);

            Object generatedValue = null;
            if (generatedValues.next()) {
                generatedValue = generatedValues.getObject(1);
            }
            return generatedValue;
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
    }

    /**
     * Performs a parameterized read query to a database to
     * find all object type E.
     *
     * @return A {@link List} implementation with all finding objects.
     * @throws DaoException Signals that an database access object exception
     *                      of some sort has occurred.
     */
    @Override
    public List<E> findAll() throws DaoException {
        Builder builder = BuilderFactory.create(getTableName());
        String query = GET_ALL_QUERY + getTableName();
        return executeQuery(query, builder);
    }

    /**
     * Performs a parameterized read query to the database, expecting
     * a single result in the form of an object of type E with the
     * specified identifier.
     *
     * @param id An object identifier in database.
     * @return An {@link Optional} implementation with object.
     * @throws DaoException Signals that an database access object exception
     *                  of some sort has occurred.
     */
    @Override
    public Optional<E> findById(final long id) throws DaoException {
        Builder builder = BuilderFactory.create(getTableName());
        String query = GET_ALL_QUERY + getTableName() + WHERE_ID_CONDITION;
        String stringId = String.valueOf(id);
        return executeQueryForSingleResult(query, builder, stringId);
    }

    /**
     * Returns database connection to {@link ConnectionPool}.
     */
    @Override
    public void close() {
        connectionPool.releaseConnection(connection);
    }

    protected abstract Object[] parseEntityToStringArray(E entity);
}
