package by.epam.alienpedia.dao;

import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.SQLException;

@Log4j2
public class TransactionManager {

    private Connection connection;

    public void beginTransaction(AbstractDao dao) {
        connection = dao.getConnection();
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            log.error("Failed to begin transaction!", e);
        }
    }

    public void endTransaction() {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            log.error("Failed to end transaction!", e);
        }

    }

    public void commit() {
        try {
            connection.commit();
        } catch (SQLException e) {
            log.error("Failed to commit transaction", e);
        }
    }

    public void rollback() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            log.error("Failed to rollback transaction!", e);
        }
    }
}
