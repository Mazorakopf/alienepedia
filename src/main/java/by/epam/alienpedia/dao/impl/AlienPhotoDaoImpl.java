package by.epam.alienpedia.dao.impl;

import by.epam.alienpedia.dao.AlienPhotoDao;
import by.epam.alienpedia.dao.builder.AlienPhotoBuilder;
import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.AlienPhoto;

import java.util.List;

public class AlienPhotoDaoImpl extends AlienPhotoDao {

    private static final int ID = 0;
    private static final int URL = 1;
    private static final int PUBLICATION_ID = 2;

    private static final String TABLE_NAME = "alien_photo";

    private static final String FIND_PHOTO_BY_ID_QUERY = "SELECT * from alien_photo where publication_id = ?";
    private static final String INSERT_QUERY = "INSERT INTO alien_photo (photo_url, publication_id) VALUES(?,?);";

    public AlienPhotoDaoImpl() {
        super();
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public long create(AlienPhoto entity) throws DaoException {
        Object[] values = parseEntityToStringArray(entity);

        return executeUpdate(INSERT_QUERY, values[URL], values[PUBLICATION_ID]);
    }

    @Override
    public long update(AlienPhoto entity) throws DaoException {
        return 0;
    }

    @Override
    public List<AlienPhoto> findAlienPhotosByPublicationId(long id) throws DaoException {
        return executeQuery(FIND_PHOTO_BY_ID_QUERY, new AlienPhotoBuilder(), id);
    }


    @Override
    protected Object[] parseEntityToStringArray(AlienPhoto entity) {
        return new Object[]{
                entity.getId(),
                entity.getUrl(),
                entity.getPublicationId()
        };
    }
}
