package by.epam.alienpedia.dao.impl;

import by.epam.alienpedia.dao.PublicationDao;
import by.epam.alienpedia.dao.builder.PublicationBuilder;
import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.Publication;

import java.util.List;

public class PublicationDaoImpl extends PublicationDao {

    private static final String TABLE_NAME = "publication";

    private static final int ID = 0;
    private static final int OWNER_ID = 1;
    private static final int DATE = 2;
    private static final int VERIFIED = 3;
    private static final int NAME = 4;
    private static final int NICKNAMES = 5;
    private static final int UNIVERSE = 6;
    private static final int ORIGIN = 7;
    private static final int POSITION = 8;
    private static final int ABOUT = 9;

    private static final String USER_PUBLICATION_QUERY = "SELECT * FROM publication WHERE owner_id = ?;";
    private static final String LIMIT_PUBLICATION_QUERY = "SELECT * FROM publication WHERE is_verified = 1"
            + " order by rating DESC LIMIT ?, ?;";
    private static final String PUBLICATION_COUNT_QUERY = "SELECT COUNT(*) FROM publication WHERE is_verified = 1;";
    private static final String INSERT_QUERY = "INSERT INTO publication (owner_id, date_of_publication, is_verified,"
            + "name, nicknames, universe, origin, position, about, rating) VALUES(?,?,0,?,?,?,?,?,?,0);";
    private static final String IS_LIKED_PUBLICATION_QUERY = "SELECT EXISTS (SELECT * FROM likes WHERE user_id = ?"
            + " AND publication_id = ?) AS 'like';";
    private static final String LIKED_PUBLICATION_QUERY = "INSERT INTO likes (user_id, publication_id) VALUES(?,?);";
    private static final String UNLIKED_PUBLICATION_QUERY = "DELETE FROM likes WHERE user_id = ? AND publication_id = ?;";
    private static final String UP_RATING_QUERY = "UPDATE publication SET rating = rating + 1 WHERE id =?;";
    private static final String DOWN_RATING_QUERY = "UPDATE publication SET rating = rating - 1 WHERE id =?;";
    private static final String UPDATE_PUBLICATION_QUERY = "UPDATE publication SET is_verified = ?, name = ?, nicknames = ?," +
            " universe = ?, origin = ?, position = ?, about = ? WHERE id = ?;";

    public PublicationDaoImpl() {
        super();
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }


    @Override
    public List<Publication> findVerifiedPublications(int position, int publicationPerPage) throws DaoException {
        Integer integerPosition = position;
        Integer integerNumber = publicationPerPage;
        return executeQuery(LIMIT_PUBLICATION_QUERY, new PublicationBuilder(), integerPosition, integerNumber);
    }

    @Override
    public List<Publication> findUserPublications(long userId) throws DaoException {
        return executeQuery(USER_PUBLICATION_QUERY, new PublicationBuilder(), userId);
    }

    @Override
    public long findNumberOfPublication() throws DaoException {
        return (Long) getGeneratedValue(PUBLICATION_COUNT_QUERY);
    }

    @Override
    public boolean isLikedPublication(long publicationId, long userId) throws DaoException {
        return ((Long) getGeneratedValue(IS_LIKED_PUBLICATION_QUERY, userId, publicationId)) == 1;
    }

    @Override
    public long likedPublication(long publicationId, long userId) throws DaoException {
        return executeUpdate(LIKED_PUBLICATION_QUERY, userId, publicationId);
    }

    @Override
    public long upPublicationRating(long publicationId) throws DaoException {
        return executeUpdate(UP_RATING_QUERY, publicationId);

    }

    @Override
    public long unLikedPublication(long publicationId, long userId) throws DaoException {
        return executeUpdate(UNLIKED_PUBLICATION_QUERY, userId, publicationId);
    }

    @Override
    public long downPublicationRating(long publicationId) throws DaoException {
        return executeUpdate(DOWN_RATING_QUERY, publicationId);
    }

    @Override
    public long create(Publication entity) throws DaoException {
        Object[] values = parseEntityToStringArray(entity);

        return executeUpdate(INSERT_QUERY, values[OWNER_ID], values[DATE], values[NAME], values[NICKNAMES],
                values[UNIVERSE], values[ORIGIN], values[POSITION], values[ABOUT]);
    }

    @Override
    public long update(Publication entity) throws DaoException {
        Object[] values = parseEntityToStringArray(entity);

        return executeUpdate(UPDATE_PUBLICATION_QUERY, values[VERIFIED], values[NAME], values[NICKNAMES],
                values[UNIVERSE], values[ORIGIN], values[POSITION], values[ABOUT], values[ID]);
    }

    @Override
    protected Object[] parseEntityToStringArray(Publication entity) {
        return new Object[]{
                entity.getId(),
                entity.getOwnerId(),
                entity.getDateOfPublication(),
                entity.isVerified(),
                entity.getName(),
                entity.getNicknames(),
                entity.getUniverse(),
                entity.getOrigin(),
                entity.getPosition().getValue(),
                entity.getAbout()
        };
    }
}
