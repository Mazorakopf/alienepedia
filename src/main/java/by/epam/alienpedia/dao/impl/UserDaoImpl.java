package by.epam.alienpedia.dao.impl;

import by.epam.alienpedia.dao.UserDao;
import by.epam.alienpedia.dao.builder.UserBuilder;
import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.model.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Optional;

public class UserDaoImpl extends UserDao {

    private static final int ID = 0;
    private static final int FIRST_NAME = 1;
    private static final int LAST_NAME = 2;
    private static final int EMAIL = 3;
    private static final int USERNAME = 4;
    private static final int ENCRYPTED_PASSWORD = 5;
    private static final int BANNED = 6;
    private static final int ROLE = 7;
    private static final int PASSWORD = 8;

    private static final String TABLE_NAME = "user";

    private static final String USERNAME_AND_PASSWORD_QUERY = "SELECT * FROM user WHERE username = ? AND password = ?";
    private static final String USERNAME_QUERY = "SELECT * FROM user WHERE username = ?";
    private static final String INSERT_QUERY = "INSERT INTO user (first_name, last_name, email, username, password," +
            " is_banned, status) VALUES(?,?,?,?,?,?,?);";
    private static final String UPDATE_USER_QUERY = "UPDATE user SET first_name = ?, last_name = ?, email = ?,"
            + " is_banned = ? WHERE id = ?;";

    public UserDaoImpl() {
        super();
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public long create(User entity) throws DaoException {
        Object[] values = parseEntityToStringArray(entity);

        return executeUpdate(INSERT_QUERY, values[FIRST_NAME], values[LAST_NAME], values[EMAIL],
                values[USERNAME], values[ENCRYPTED_PASSWORD], values[BANNED], values[ROLE]);
    }

    @Override
    public long update(User entity) throws DaoException {
        Object[] values = parseEntityToStringArray(entity);

        return executeUpdate(UPDATE_USER_QUERY, values[FIRST_NAME], values[LAST_NAME], values[EMAIL],
                values[BANNED], values[ID]);
    }

    @Override
    public Optional<User> findUserByLoginAndPassword(String login, String password) throws DaoException {
        String encryptedPassword = DigestUtils.sha256Hex(password);
        return executeQueryForSingleResult(USERNAME_AND_PASSWORD_QUERY, new UserBuilder(), login, encryptedPassword);
    }

    @Override
    public Optional<User> findUserByLogin(String login) throws DaoException {
        return executeQueryForSingleResult(USERNAME_QUERY, new UserBuilder(), login);
    }

    @Override
    protected Object[] parseEntityToStringArray(User entity) {
        return new Object[]{
                entity.getId(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getEmail(),
                entity.getUserName(),
                DigestUtils.sha256Hex(entity.getPassword()),
                entity.isBanned(),
                entity.getRole().getValue(),
                entity.getPassword()
        };
    }
}
