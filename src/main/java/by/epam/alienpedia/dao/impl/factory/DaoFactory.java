package by.epam.alienpedia.dao.impl.factory;

import by.epam.alienpedia.dao.AlienPhotoDao;
import by.epam.alienpedia.dao.PublicationDao;
import by.epam.alienpedia.dao.UserDao;
import by.epam.alienpedia.dao.impl.AlienPhotoDaoImpl;
import by.epam.alienpedia.dao.impl.PublicationDaoImpl;
import by.epam.alienpedia.dao.impl.UserDaoImpl;

/**
 * Provides creator of dao implementation class.
 */
public final class DaoFactory {

    private DaoFactory() {
    }

    /**
     * @return an {@link UserDaoImpl} object with connection to database.
     */
    public static UserDao newUserDao() {
        return new UserDaoImpl();
    }

    /**
     * @return an {@link PublicationDaoImpl} object with connection to database.
     */
    public static PublicationDao newPublicationDao() {
        return new PublicationDaoImpl();
    }

    /**
     * @return an {@link AlienPhotoDaoImpl} object with connection to database.
     */
    public static AlienPhotoDao newAlienPhotoDao() {
        return new AlienPhotoDaoImpl();
    }

}
