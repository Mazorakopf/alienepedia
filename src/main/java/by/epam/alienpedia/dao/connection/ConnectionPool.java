package by.epam.alienpedia.dao.connection;

import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Properties;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Designed to create, work and store database connections
 * in the singleton style.
 */
@Log4j2
public final class ConnectionPool {

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final String PROPERTIES_FILE_FOR_CONNECTION_POOL = "database.properties";

    /**
     * Names of the properties for initialization {@code ConnectionPool} fields.
     */
    private static final String DB_URL = "db.url";
    private static final String DB_USER = "db.user";
    private static final String DB_PASSWORD = "db.password";
    private static final String DB_DRIVER = "db.driver";
    private static final String DB_POOL_SIZE = "db.poolSize";

    /**
     * Flag to check the existence of the instance {@code ConnectionPool}.
     */
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
    private static ConnectionPool instance;

    /**
     * The queue with available connections.
     */
    private Deque<Connection> connections;
    /**
     * Semaphore to block access if there are no available connections in the queue.
     */
    private Semaphore semaphore;

    private String url;
    private String user;
    private String password;
    private int poolSize;

    /**
     * Constructor with initialization of fields.
     */
    private ConnectionPool() {
        initializeData();
        createConnections();
    }

    /**
     * Designed to get a single instance of a class with a double-checked
     * locking.
     *
     * @return {@code ConnectionPool} class instance.
     */
    public static ConnectionPool getInstance() {
        if (!instanceCreated.get()) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    instanceCreated.set(true);
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    /**
     * Designed for thread-safe retrieve database connection from storage.
     *
     * @return A {@link Connection} object that provide connection to database
     */
    public Connection getConnection() {
        try {
            LOCK.lock();
            semaphore.acquire();
            return connections.pop();
        } catch (InterruptedException e) {
            throw new IllegalArgumentException();
        } finally {
            LOCK.unlock();
        }
    }

    /**
     * Returns connection into the storage.
     *
     * @param connection Connection that should bu returned.
     */
    public void releaseConnection(final Connection connection) {
        try {
            LOCK.lock();
            connections.push(connection);
            semaphore.release();
        } finally {
            LOCK.unlock();
        }
    }

    /**
     * Designed to close connections.
     *
     * @throws SQLException Signals that an sql exception of some sort has occurred.
     */
    public void shutDown() throws SQLException {
        Connection connection;
        while (!connections.isEmpty()) {
            connection = connections.poll();

            if (!connection.getAutoCommit()) {
                connection.commit();
            }

            ((PooledConnection) connection).reallyClose();
        }
    }

    /**
     * Designed for database data initialization. Data is obtained from
     * properties file.
     */
    private void initializeData() {
        String driver;
        connections = new ArrayDeque<>();

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream inputStream = classLoader
                    .getResourceAsStream(PROPERTIES_FILE_FOR_CONNECTION_POOL);

            Properties initProperty = new Properties();
            initProperty.load(inputStream);

            url = initProperty.getProperty(DB_URL);
            user = initProperty.getProperty(DB_USER);
            password = initProperty.getProperty(DB_PASSWORD);
            driver = initProperty.getProperty(DB_DRIVER);
            String poolSizeString = initProperty.getProperty(DB_POOL_SIZE);
            poolSize = Integer.parseInt(poolSizeString);

            Class.forName(driver);

        } catch (IOException e) {
            throw new IllegalArgumentException("File with properties not found! " + e.getMessage(), e);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("Driver is not found! " + e.getMessage(), e);
        }
        semaphore = new Semaphore(poolSize);
    }

    /**
     * Creates {@link Connection} and wraps it in {@link PooledConnection},
     * then put them into storage. Uses structural design pattern Proxy.
     */
    private void createConnections() {
        for (int i = 0; i < poolSize; i++) {
            try {
                Connection connection = DriverManager.getConnection(url, user, password);
                PooledConnection proxyConnection = new PooledConnection(connection);
                connections.push(proxyConnection);
            } catch (SQLException e) {
                log.error(e.getMessage(), e);
            }
        }
        if (connections.isEmpty()) {
            throw new IllegalArgumentException("Connections are not created!");
        }
    }
}
