/**
 * This package contains classes that work with the {@link java.sql.Connection}
 * and provide access to the database.
 */
package by.epam.alienpedia.dao.connection;