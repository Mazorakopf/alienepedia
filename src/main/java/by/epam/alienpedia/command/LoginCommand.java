package by.epam.alienpedia.command;

import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.entity.User;
import by.epam.alienpedia.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Designed to perform a login process.
 */
public class LoginCommand extends AbstractCommand {

    /**
     * Name of the request attributes.
     */
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String ERROR_LOGIN_MESSAGE = "errorLoginMessage";

    private static final String LOGIN_PAGE = "/login";
    private static final String COMMAND_MAIN = "controller?command=main";

    private static final String AUTHENTICATION_FAILED_MESSAGE = "Authentication failed!";
    private static final String ACCOUNT_HAS_BEEN_BANNED_MESSAGE = "Your account has been banned!";

    /**
     * Process the POST request, login and generates a result of processing
     * in the form of {@link by.epam.alienpedia.command.CommandResult} object.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executePost(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        UserService userService = new UserService();

        String login = request.getParameter(USERNAME);
        String password = request.getParameter(PASSWORD);

        Optional<User> user;
        try {
            user = userService.login(login, password);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }


        if (!user.isPresent()) {
            request.setAttribute(ERROR_LOGIN_MESSAGE, AUTHENTICATION_FAILED_MESSAGE);
            return new CommandResult(LOGIN_PAGE, false);
        }

        User userEntity = user.get();
        if (userEntity.isBanned()) {
            request.setAttribute(ERROR_LOGIN_MESSAGE, ACCOUNT_HAS_BEEN_BANNED_MESSAGE);
            return new CommandResult(LOGIN_PAGE, false);
        }

        setUserIdIntoSession(request, userEntity);

        return new CommandResult(COMMAND_MAIN, true);
    }
}
