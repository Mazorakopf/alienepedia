package by.epam.alienpedia.command.user;

import by.epam.alienpedia.command.CommandResult;
import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.entity.Publication;
import by.epam.alienpedia.model.entity.User;
import by.epam.alienpedia.service.PublicationService;
import by.epam.alienpedia.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;


/**
 * Designed to display profile page.
 */
public class ProfileCommand extends UserAbstractCommand {

    /**
     * Name of the request attributes.
     */
    private static final String USER = "user";
    private static final String PUBLICATIONS = "publications";

    private static final String USER_PROFILE_PAGE = "/WEB-INF/jsp/user/userProfile.jsp";

    /**
     * Generates a result of request processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object with profile  page.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executeGet(final HttpServletRequest request,
                                    final HttpServletResponse response) throws CommandException {

        long userId = getUserIdFromSession(request);

        List<Publication> publications = getUserPublications(userId, new PublicationService());

        Optional<User> user = getUserInformation(userId, new UserService());

        user.ifPresent(aUser -> request.setAttribute(USER, aUser));
        request.setAttribute(PUBLICATIONS, publications);

        return new CommandResult(USER_PROFILE_PAGE, false);
    }

    private Optional<User> getUserInformation(final long userId,
                                              final UserService userService) throws CommandException {
        Optional<User> user;
        try {
            user = userService.findById(userId);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
        return user;
    }

    private List<Publication> getUserPublications(final long userId,
                                                  final PublicationService publicationService) throws CommandException {
        List<Publication> publications;
        try {
            publications = publicationService.findUserPublication(userId);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
        return publications;
    }
}
