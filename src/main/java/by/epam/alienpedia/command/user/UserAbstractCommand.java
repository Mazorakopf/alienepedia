package by.epam.alienpedia.command.user;

import by.epam.alienpedia.command.AbstractCommand;
import by.epam.alienpedia.model.entity.Role;

import java.util.EnumSet;
import java.util.Set;

abstract class UserAbstractCommand extends AbstractCommand {

    private static final Set<Role> ALLOWED_ROLES = EnumSet.of(Role.USER);

    @Override
    public boolean isAllowedRoles(Role role) {
        return ALLOWED_ROLES.contains(role);
    }
}
