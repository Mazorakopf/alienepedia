package by.epam.alienpedia.command.user;

import by.epam.alienpedia.command.CommandResult;
import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.entity.User;
import by.epam.alienpedia.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditProfileCommand extends UserAbstractCommand {

    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String EMAIL = "email";

    private static final String EMPTY_STRING = "";

    private static final String PROFILE_PAGE = "controller?command=profile";

    @Override
    public CommandResult executePost(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        long userId = getUserIdFromSession(request);

        String firstName = request.getParameter(FIRST_NAME);
        String lastName = request.getParameter(LAST_NAME);
        String email = request.getParameter(EMAIL);

        UserService userService = new UserService();
        try {
            User user = userService.findById(userId).get();

            if (!firstName.equals(EMPTY_STRING)) {
                user.setFirstName(firstName);
            }
            if (!lastName.equals(EMPTY_STRING)) {
                user.setLastName(lastName);
            }
            if (!email.equals(EMPTY_STRING)) {
                user.setEmail(email);
            }
            userService.updateUser(user);

        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

        return new CommandResult(PROFILE_PAGE, true);
    }
}
