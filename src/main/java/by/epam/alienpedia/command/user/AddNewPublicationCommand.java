package by.epam.alienpedia.command.user;

import by.epam.alienpedia.command.CommandResult;
import by.epam.alienpedia.command.builder.PublicationDtoBuilder;
import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.dto.PublicationDto;
import by.epam.alienpedia.service.PublicationDtoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Designed to complete add a publication process.
 */
public class AddNewPublicationCommand extends UserAbstractCommand {

    private static final String COMMAND_PROFILE = "controller?command=profile";

    /**
     * Process the POST request, add a publication and generates a result of processing
     * in the form of {@link by.epam.alienpedia.command.CommandResult} object.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request.
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client.
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executePost(final HttpServletRequest request,
                                     final HttpServletResponse response) throws CommandException {

        PublicationDto publicationDto = new PublicationDtoBuilder().build(request);

        PublicationDtoService publicationDtoService = new PublicationDtoService();
        try {
            publicationDtoService.addNewPublication(publicationDto);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

        return new CommandResult(COMMAND_PROFILE, true);
    }
}
