package by.epam.alienpedia.command.user;

import by.epam.alienpedia.command.AbstractCommand;
import by.epam.alienpedia.command.CommandResult;
import by.epam.alienpedia.controller.util.PageManager;
import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.service.PublicationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UnlikeCommand extends AbstractCommand {

    @Override
    public CommandResult executePost(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        long userId = getUserIdFromSession(request);

        long publicationId = getPublicationIdFromRequest(request);

        PublicationService publicationService = new PublicationService();
        try {
            publicationService.unLikedPublication(publicationId, userId);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

        String page = PageManager.getRefererPage(request);
        return new CommandResult(page, true);
    }
}
