package by.epam.alienpedia.command.factory;

import by.epam.alienpedia.command.*;
import by.epam.alienpedia.command.admin.*;
import by.epam.alienpedia.command.guest.RegistrationCommand;
import by.epam.alienpedia.command.user.*;


/**
 * Designed to build an object of type {@link Command}.
 */
public final class CommandFactory {

    private CommandFactory() {
    }

    public static Command create(final String command) {

        String commandInUpCase = command.toUpperCase();
        CommandEnum commandEnum = CommandEnum.valueOf(commandInUpCase);

        Command resultCommand;
        switch (commandEnum) {
            case MAIN:
                resultCommand = new MainCommand();
                break;
            case LANGUAGE:
                resultCommand = new LanguageCommand();
                break;
            case ALIENINFO:
                resultCommand = new PublicationInfoCommand();
                break;
            case LOGIN:
                resultCommand = new LoginCommand();
                break;
            case REGISTRATION:
                resultCommand = new RegistrationCommand();
                break;
            case LOGOUT:
                resultCommand = new LogoutCommand();
                break;
            case PROFILE:
                resultCommand = new ProfileCommand();
                break;
            case EDITPROFILE:
                resultCommand = new EditProfileCommand();
                break;
            case ADDNEWPUBLICATION:
                resultCommand = new AddNewPublicationCommand();
                break;
            case LIKE:
                resultCommand = new LikeCommand();
                break;
            case UNLIKE:
                resultCommand = new UnlikeCommand();
                break;
            case MANAGEMENT:
                resultCommand = new ManagementCommand();
                break;
            case VERIFIEDPUBLICATION:
                resultCommand = new VerifiedPublicationCommand();
                break;
            case BLOCKEDPUBLICATION:
                resultCommand = new BlockedPublicationCommand();
                break;
            case BANUSER:
                resultCommand = new BanCommand();
                break;
            case UNBANUSER:
                resultCommand = new UnBanCommand();
                break;
            default:
                throw new IllegalArgumentException("Invalid command"
                        + commandEnum);
        }
        return resultCommand;
    }
}
