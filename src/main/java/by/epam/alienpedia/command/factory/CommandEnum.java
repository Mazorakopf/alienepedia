package by.epam.alienpedia.command.factory;

/**
 * Storage of commands
 */
public enum CommandEnum {

    MAIN("main"),
    ALIENINFO("alienInfo"),
    LOGIN("login"),
    REGISTRATION("registration"),
    LOGOUT("logout"),
    PROFILE("profile"),
    LANGUAGE("language"),
    EDITPROFILE("editProfile"),
    ADDNEWPUBLICATION("addNewPublication"),
    LIKE("like"),
    UNLIKE("unlike"),
    MANAGEMENT("management"),
    VERIFIEDPUBLICATION("verifiedPublication"),
    BLOCKEDPUBLICATION("blockedPublication"),
    BANUSER("banUser"),
    UNBANUSER("unBanUser");

    private String value;

    CommandEnum(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
