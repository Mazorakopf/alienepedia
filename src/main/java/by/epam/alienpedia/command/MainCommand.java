package by.epam.alienpedia.command;

import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.dto.PublicationDto;
import by.epam.alienpedia.service.PublicationDtoService;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * {@code MainCommand} designed to display the main page and its contents.
 */
@Log4j2
public class MainCommand extends AbstractCommand {

    private static final String MAIN_PAGE = "/WEB-INF/jsp/main.jsp";

    /**
     * Number of publication per page.
     */
    private static final int PUBLICATION_PER_PAGE = 2;

    /**
     * Name of the request attribute.
     */
    private static final String CURRENT_PAGE = "page";
    private static final String PUBLICATIONS = "publications";
    private static final String PAGE_COUNT = "pageCount";

    /**
     * Process the request, prepare main page and generates a result of
     * processing in the form of {@link by.epam.alienpedia.command.CommandResult}
     * object.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executeGet(final HttpServletRequest request,
                                    final HttpServletResponse response) throws CommandException {
        PublicationDtoService publicationDtoService = new PublicationDtoService();

        int page = getCurrentPage(request);

        List<PublicationDto> publicationDtos =
                getVerifiedPublicationOnCurrentPage(page, publicationDtoService);

        long pageCount = getPageCount(publicationDtoService);

        Long userId = getUserIdFromSession(request);
        if (userId != null) {
            setUserLikes(publicationDtoService, publicationDtos, userId);
        }

        request.setAttribute(PUBLICATIONS, publicationDtos);
        request.setAttribute(PAGE_COUNT, pageCount);
        request.setAttribute(CURRENT_PAGE, page);

        return new CommandResult(MAIN_PAGE, false);
    }

    /**
     * @param publicationDtoService A {@link PublicationDtoService} object.
     * @return Required number of pages to display all publications.
     * @throws CommandException When {@link ServiceException} is caught.
     */
    private long getPageCount(final PublicationDtoService publicationDtoService) throws CommandException {
        long pageCount;
        try {
            pageCount = publicationDtoService.findPublicationPagesCount(PUBLICATION_PER_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
        return pageCount;
    }

    /**
     * @param page                  Number of current main page.
     * @param publicationDtoService A {@link PublicationDtoService} object.
     * @return List of verified publication for display on current page.
     * @throws CommandException When {@link ServiceException} is caught.
     */
    private List<PublicationDto> getVerifiedPublicationOnCurrentPage(final int page,
                                                                     final PublicationDtoService publicationDtoService) throws CommandException {
        List<PublicationDto> publicationDtos;
        try {
            publicationDtos = publicationDtoService.findVerifiedPublication(page, PUBLICATION_PER_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
        return publicationDtos;
    }

    /**
     * @param publicationDtoService A {@link PublicationDtoService} object.
     * @param publicationDtos       List of {@link PublicationDto} objects.
     * @param userId                User id that is stored in the session.
     * @throws CommandException When {@link ServiceException} is caught.
     */
    private void setUserLikes(final PublicationDtoService publicationDtoService,
                              final List<PublicationDto> publicationDtos,
                              final long userId) throws CommandException {
        try {
            publicationDtoService.findLikedPublications(publicationDtos, userId);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    /**
     * @param request An {@link HttpServletRequest} object that contains
     *                client request
     * @return Number of current main page.
     */
    private int getCurrentPage(final HttpServletRequest request) {
        String pageStr = request.getParameter(CURRENT_PAGE);
        return (pageStr == null || pageStr.isEmpty())
                ? 1 : Integer.parseInt(pageStr);
    }
}
