package by.epam.alienpedia.command.builder;

import by.epam.alienpedia.model.entity.Role;
import by.epam.alienpedia.model.entity.User;

import javax.servlet.http.HttpServletRequest;

/**
 * Designed to build an object of type {@link by.epam.alienpedia.model.entity.User}
 * with specified characteristics.
 */
public class UserBuilder implements Builder<User> {

    private static final int DEFAULT_ID = 0;
    private static final boolean DEFAULT_BANNED_STATUS = false;
    private static final Role DEFAULT_ROLE = Role.USER;
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String EMAIL = "email";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    /**
     * Builds an object of type User with properties.
     *
     * @param request An {@link HttpServletRequest} object that contains
     *                client request
     * @return An {@link User} object.
     */
    @Override
    public User build(final HttpServletRequest request) {

        String firstName = request.getParameter(FIRST_NAME);
        String lastName = request.getParameter(LAST_NAME);
        String username = request.getParameter(USERNAME);
        String password = request.getParameter(PASSWORD);
        String email = request.getParameter(EMAIL);

        return new User(DEFAULT_ID, firstName, lastName, email, username, password,
                DEFAULT_BANNED_STATUS, DEFAULT_ROLE);
    }
}
