package by.epam.alienpedia.command.builder;

import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.dto.PublicationDto;
import by.epam.alienpedia.model.entity.AlienPhoto;
import by.epam.alienpedia.model.entity.Position;
import by.epam.alienpedia.model.entity.Publication;
import by.epam.alienpedia.service.util.FileUploader;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Designed to build an object of type {@link by.epam.alienpedia.model.dto.PublicationDto}
 * with specified characteristics.
 */
public class PublicationDtoBuilder implements Builder<PublicationDto> {

    private static final String ALIEN_NAME = "alienName";
    private static final String ALIEN_NICKNAMES = "alienNicknames";
    private static final String ALIEN_UNIVERSE = "alienUniverse";
    private static final String ALIEN_ORIGIN = "alienOrigin";
    private static final String ALIEN_POSITION = "alienPosition";
    private static final String ALIEN_ABOUT = "alienAbout";
    private static final String ENCODING = "UTF-8";
    private static final String JAVAX_SERVLET_CONTEXT_TEMPDIR = "javax.servlet.context.tempdir";
    private static final String ID = "userId";

    /**
     * Builds an object of type PublicationDto with properties.
     *
     * @param request An {@link HttpServletRequest} object that contains
     *                client request
     * @return A {@link PublicationDto} object.
     * @throws CommandException Throws when {@link com.google.protobuf.ServiceException},
     *                          {@link FileUploadException} is caught or the request is not sent
     *                          with the help of multipart encoding.
     */
    @Override
    public PublicationDto build(HttpServletRequest request) throws CommandException {

        if (!ServletFileUpload.isMultipartContent(request)) {
            throw new CommandException("It isn't multipart form.");
        }

        Publication publication = new Publication();

        HttpSession session = request.getSession();
        long ownerId = (Long) session.getAttribute(ID);
        publication.setOwnerId(ownerId);

        List<String> publicationPhotoPath = new ArrayList<>();
        try {
            List<FileItem> fileItems = uploadFileItems(request);

            FileUploader uploader = FileUploader.getInstance();
            for (final FileItem item : fileItems) {
                if (item.isFormField()) {
                    buildPublication(item, publication);
                } else {
                    String filePath = uploader.upload(item);
                    publicationPhotoPath.add(filePath);
                }
            }
        } catch (FileUploadException | ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

        publication.setDateOfPublication(LocalDateTime.now());

        List<AlienPhoto> alienPhotos = buildAlienPhoto(publicationPhotoPath);

        return new PublicationDto(publication, alienPhotos);
    }

    private void buildPublication(FileItem item, Publication publication) throws CommandException {

        String paramName = item.getFieldName();
        String paramValue;
        try {
            paramValue = item.getString(ENCODING);
        } catch (UnsupportedEncodingException e) {
            throw new CommandException(e.getMessage(), e);
        }

        switch (paramName) {
            case ALIEN_NAME:
                publication.setName(paramValue);
                break;
            case ALIEN_NICKNAMES:
                publication.setNicknames(paramValue);
                break;
            case ALIEN_UNIVERSE:
                publication.setUniverse(paramValue);
                break;
            case ALIEN_ORIGIN:
                publication.setOrigin(paramValue);
                break;
            case ALIEN_POSITION:
                Position position = Position.valueOf(paramValue);
                publication.setPosition(position);
                break;
            case ALIEN_ABOUT:
                publication.setAbout(paramValue);
                break;
            default:
                throw new IllegalArgumentException("Unknown type of parameter!");
        }
    }

    private List<AlienPhoto> buildAlienPhoto(List<String> photosPaths) {

        List<AlienPhoto> alienPhotos = new ArrayList<>();
        for (String photoPath : photosPaths) {
            AlienPhoto alienPhoto = new AlienPhoto();

            alienPhoto.setUrl(photoPath);

            alienPhotos.add(alienPhoto);
        }
        return alienPhotos;
    }

    private List<FileItem> uploadFileItems(HttpServletRequest request) throws FileUploadException {
        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
        ServletContext servletContext = request.getServletContext();
        File repository = (File) servletContext.getAttribute(JAVAX_SERVLET_CONTEXT_TEMPDIR);
        fileItemFactory.setRepository(repository);

        ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
        return fileUpload.parseRequest(request);
    }
}
