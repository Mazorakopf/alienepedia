package by.epam.alienpedia.command.builder;

import by.epam.alienpedia.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Designed to determine the behavior of the implementing class in the form
 * of construction an object of type E with specified characteristics.
 *
 * @param <E> - type of object.
 */
public interface Builder<E> {

    /**
     * Builds an object of type E with properties.
     *
     * @param request An {@link HttpServletRequest} object that contains
     *                client request
     * @return Returns built object type E.
     * @throws CommandException Throws when {@link com.google.protobuf.ServiceException} is caught.
     */
    E build(HttpServletRequest request) throws CommandException;
}
