package by.epam.alienpedia.command;

import by.epam.alienpedia.controller.util.PageManager;
import by.epam.alienpedia.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Designed to change language.
 */
public class LanguageCommand extends AbstractCommand {

    private static final String RU = "RU";
    private static final String EN = "EN";
    private static final String DE = "DE";

    /**
     * Name of the request and session attribute that stores the languages.
     */
    private static final String LANGUAGE = "language";
    private static final String NEXT_LANGUAGE = "nextLanguage";

    private static final String UNKNOWN_LANGUAGE_MESSAGE = "Unknown language!";

    /**
     * Process the GET request, change language and generates a result of processing
     * in the form of {@link by.epam.alienpedia.command.CommandResult} object.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executeGet(final HttpServletRequest request,
                                    final HttpServletResponse response) throws CommandException {
        String language = request.getParameter(LANGUAGE);
        HttpSession session = request.getSession();

        session.setAttribute(LANGUAGE, language);

        String nextLanguage = findNextLanguage(language);

        request.setAttribute(NEXT_LANGUAGE, nextLanguage);
        session.setAttribute(NEXT_LANGUAGE, nextLanguage);

        String currentPage = PageManager.getRefererPage(request);
        return new CommandResult(currentPage, true);
    }

    /**
     * @param language Current page language.
     * @return Next page language.
     */
    private String findNextLanguage(final String language) {
        String nextLanguage;
        switch (language) {
            case RU:
                nextLanguage = EN;
                break;
            case EN:
                nextLanguage = DE;
                break;
            case DE:
                nextLanguage = RU;
                break;
            default:
                throw new IllegalArgumentException(UNKNOWN_LANGUAGE_MESSAGE);
        }
        return nextLanguage;
    }
}
