package by.epam.alienpedia.command.guest;

import by.epam.alienpedia.command.AbstractCommand;
import by.epam.alienpedia.model.entity.Role;

import java.util.EnumSet;
import java.util.Set;

/**
 * Designed as an abstract class for inheriting all guest commands.
 */
abstract class GuestAbstractCommand extends AbstractCommand {

    /**
     * Allowed user roles to run the command.
     */
    private static final Set<Role> ALLOWED_ROLES = EnumSet.of(Role.GUEST);

    /**
     * @param role User role in the current session.
     * @return {@code true} if the role is allowed for this command,
     * otherwise {@code false}.
     */
    @Override
    public boolean isAllowedRoles(Role role) {
        return ALLOWED_ROLES.contains(role);
    }
}
