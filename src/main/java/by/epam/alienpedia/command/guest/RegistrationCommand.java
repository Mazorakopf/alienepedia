package by.epam.alienpedia.command.guest;

import by.epam.alienpedia.command.CommandResult;
import by.epam.alienpedia.command.builder.UserBuilder;
import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.entity.User;
import by.epam.alienpedia.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Designed to perform a registration process.
 */
public class RegistrationCommand extends GuestAbstractCommand {

    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String CONFIRM_PASSWORD = "rePassword";

    private static final String PASSWORD_DONT_MATCH = "Password don't match!";
    private static final String THE_USERNAME_IS_ALREADY_TAKEN = "The username is already taken!";

    private static final String MAIN_COMMAND = "controller?command=main";
    private static final String REGISTRATION_PAGE = "registration";

    /**
     * Process the POST request, registration and generates a result of processing
     * in the form of {@link by.epam.alienpedia.command.CommandResult} object.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executePost(final HttpServletRequest request,
                                     final HttpServletResponse response) throws CommandException {
        CommandResult result = new CommandResult(REGISTRATION_PAGE, true);

        User user = new UserBuilder().build(request);
        String confirmPassword = request.getParameter(CONFIRM_PASSWORD);

        try {
            UserService userService = new UserService();
            HttpSession session = request.getSession();

            boolean everythingIsGood = true;
            if (userService.checkUsername(user.getUserName())) {
                session.setAttribute(ERROR_MESSAGE, THE_USERNAME_IS_ALREADY_TAKEN);
                everythingIsGood = false;
            }

            String password = user.getPassword();
            if (!password.equals(confirmPassword)) {
                session.setAttribute(ERROR_MESSAGE, PASSWORD_DONT_MATCH);
                everythingIsGood = false;
            }

            if (everythingIsGood) {
                user.setId(userService.registerUser(user));
                setUserIdIntoSession(request, user);
                result = new CommandResult(MAIN_COMMAND, true);
            }

        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

        return result;
    }
}
