package by.epam.alienpedia.command;

import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.model.entity.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Designed to determine the behavior of the implementing class in the form
 * of command to processing the request and form the response.
 */
public interface Command {

    /**
     * Processes the POST request and forms the answer in the form
     * {@link by.epam.alienpedia.command.CommandResult}.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    CommandResult executeGet(HttpServletRequest request, HttpServletResponse response) throws CommandException;

    /**
     * Processes the GET request and forms the answer in the form
     * {@link by.epam.alienpedia.command.CommandResult}.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                client request
     * @param response An {@link HttpServletResponse} object that contains
     *                the response the servlet sends to the client
     * @return Returns result of processing in the form of
     *              {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *              {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    CommandResult executePost(HttpServletRequest request, HttpServletResponse response) throws CommandException;

    /**
     * @param role User role in the current session.
     * @return {@code true} if the role is allowed for this command,
     *              otherwise {@code false}.
     */
    boolean isAllowedRoles(Role role);
}
