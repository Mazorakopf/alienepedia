package by.epam.alienpedia.command.admin;

import by.epam.alienpedia.command.AbstractCommand;
import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.entity.Publication;
import by.epam.alienpedia.model.entity.Role;
import by.epam.alienpedia.model.entity.User;
import by.epam.alienpedia.service.PublicationService;
import by.epam.alienpedia.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;

/**
 * Designed as an abstract class for inheriting all admin commands.
 */
abstract class AdminAbstractCommand extends AbstractCommand {

    /**
     * Allowed user roles to run the command.
     */
    private static final Set<Role> ALLOWED_ROLES = EnumSet.of(Role.ADMIN);

    /**
     * @param role User role in the current session.
     * @return {@code true} if the role is allowed for this command,
     * otherwise {@code false}.
     */
    @Override
    public boolean isAllowedRoles(final Role role) {
        return ALLOWED_ROLES.contains(role);
    }

    /**
     * @param request An {@link HttpServletRequest} object that contains
     *                client request.
     * @param banned  {@code true} if the user is banned, otherwise {@code false}.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    void setUserBanned(final HttpServletRequest request,
                       final boolean banned) throws CommandException {
        UserService userService = new UserService();

        long userId = getUserIdFromRequest(request);

        Optional<User> user;
        try {
            user = userService.findById(userId);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

        if (user.isPresent()) {
            User userEntity = user.get();
            userEntity.setBanned(banned);
            try {
                userService.updateUser(userEntity);
            } catch (ServiceException e) {
                throw new CommandException(e.getMessage(), e);
            }
        }
    }

    /**
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request.
     * @param verified {@code true} if the user is verified, otherwise {@code false}.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    void setPublicationVerified(final HttpServletRequest request,
                                final boolean verified) throws CommandException {
        PublicationService publicationService = new PublicationService();

        long publicationId = getPublicationIdFromRequest(request);

        Optional<Publication> publication;
        try {
            publication = publicationService.findById(publicationId);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

        if (publication.isPresent()) {
            Publication publicationEntity = publication.get();
            publicationEntity.setVerified(verified);
            try {
                publicationService.updatePublication(publicationEntity);
            } catch (ServiceException e) {
                throw new CommandException(e.getMessage(), e);
            }
        }
    }
}
