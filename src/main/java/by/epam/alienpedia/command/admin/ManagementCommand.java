package by.epam.alienpedia.command.admin;

import by.epam.alienpedia.command.CommandResult;
import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.entity.Publication;
import by.epam.alienpedia.model.entity.User;
import by.epam.alienpedia.service.PublicationService;
import by.epam.alienpedia.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Designed to display admin management page.
 */
public class ManagementCommand extends AdminAbstractCommand {

    /**
     * Names of request attributes.
     */
    private static final String USERS = "users";
    private static final String PUBLICATIONS = "publications";

    private static final String ADMIN_MANAGEMENT_PAGE = "/WEB-INF/jsp/admin/managementPage.jsp";

    /**
     * Generates a result of request processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object with management page.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request.
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client.
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executeGet(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        UserService userService = new UserService();
        List<User> users;
        try {
            users = userService.findAll();
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

        PublicationService publicationService = new PublicationService();
        List<Publication> publications;
        try {
            publications = publicationService.findAllPublication();
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

        request.setAttribute(USERS, users);
        request.setAttribute(PUBLICATIONS, publications);

        return new CommandResult(ADMIN_MANAGEMENT_PAGE, false);
    }
}
