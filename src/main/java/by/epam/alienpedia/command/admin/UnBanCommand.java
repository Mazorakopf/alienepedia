package by.epam.alienpedia.command.admin;

import by.epam.alienpedia.command.CommandResult;
import by.epam.alienpedia.controller.util.PageManager;
import by.epam.alienpedia.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Designed to perform the process of unlocking a user.
 */
public class UnBanCommand extends AdminAbstractCommand {

    /**
     * Process the POST request, unlocks the user and generates a result of
     * processing in the form of {@link by.epam.alienpedia.command.CommandResult} object.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request.
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client.
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executePost(HttpServletRequest request, HttpServletResponse response) throws CommandException {

        setUserBanned(request, false);

        String page = PageManager.getRefererPage(request);
        return new CommandResult(page, true);
    }
}
