package by.epam.alienpedia.command;

import by.epam.alienpedia.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Designed to complete sign out process.
 */
public class LogoutCommand extends AbstractCommand {

    /**
     * Name of the session attributes that stores information about user.
     */
    private static final String ID = "userId";
    private static final String ROLE = "role";

    private static final String COMMAND_MAIN = "/controller?command=main";

    /**
     * Process the GET request, log out from profile and generates a result of
     * processing in the form of {@link by.epam.alienpedia.command.CommandResult} object.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executeGet(final HttpServletRequest request,
                                    final HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();

        session.removeAttribute(ID);
        session.removeAttribute(ROLE);

        return new CommandResult(COMMAND_MAIN, false);
    }
}
