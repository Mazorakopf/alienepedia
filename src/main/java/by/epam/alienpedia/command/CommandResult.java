package by.epam.alienpedia.command;

import lombok.AllArgsConstructor;
import lombok.Data;


/**
 * Designed to display the result of the processing command.
 */
@Data
@AllArgsConstructor
public class CommandResult {

    private String page;
    private boolean isRedirect;
}
