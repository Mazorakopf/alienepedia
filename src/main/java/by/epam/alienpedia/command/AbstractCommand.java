package by.epam.alienpedia.command;

import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.model.entity.Role;
import by.epam.alienpedia.model.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.EnumSet;
import java.util.Set;

/**
 *
 */
public abstract class AbstractCommand implements Command {

    /**
     * Allowed user roles to run the command.
     */
    private static final Set<Role> ALLOWED_ROLES = EnumSet.allOf(Role.class);

    /**
     * Name of the request attributes.
     */
    private static final String ID = "userId";
    private static final String ALIEN_ID = "alienId";
    private static final String ROLE = "role";

    private static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";

    /**
     * Process the GET request, generates a result of processing
     * in the form of {@link by.epam.alienpedia.command.CommandResult} object.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executeGet(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        return new CommandResult(ERROR_PAGE, false);
    }

    /**
     * Process the POST request, generates a result of processing
     * in the form of {@link by.epam.alienpedia.command.CommandResult} object.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                client request.
     * @param response An {@link HttpServletResponse} object that contains
     *                the response the servlet sends to the client.
     * @return Returns result of processing in the form of
     *              {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *              {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executePost(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        return new CommandResult(ERROR_PAGE, false);
    }

    /**
     * @param role User role in the current session.
     * @return {@code true} if the role is allowed for this command,
     *              otherwise {@code false}.
     */
    @Override
    public boolean isAllowedRoles(Role role) {
        return ALLOWED_ROLES.contains(role);
    }

    /**
     * @param request An {@link HttpServletRequest} object that contains
     *                client request.
     * @return User id in the current session.
     */
    protected Long getUserIdFromSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (Long) session.getAttribute(ID);
    }

    /**
     * @param request An {@link HttpServletRequest} object that contains
     *                client request.
     * @param user    {@link User} object to retrieve the role and id.
     */
    protected void setUserIdIntoSession(HttpServletRequest request, User user) {
        long id = user.getId();
        Role role = user.getRole();

        HttpSession session = request.getSession();
        session.setAttribute(ID, id);
        session.setAttribute(ROLE, role);

    }

    /**
     * @param request An {@link HttpServletRequest} object that contains
     *                client request.
     * @return Publication id.
     */
    protected long getPublicationIdFromRequest(HttpServletRequest request) {
        String idString = request.getParameter(ALIEN_ID);
        return Long.parseLong(idString);
    }

    /**
     * @param request An {@link HttpServletRequest} object that contains
     *                client request.
     * @return User id from request.
     */
    protected long getUserIdFromRequest(HttpServletRequest request) {
        String userIdString = request.getParameter(ID);
        return Long.parseLong(userIdString);
    }
}
