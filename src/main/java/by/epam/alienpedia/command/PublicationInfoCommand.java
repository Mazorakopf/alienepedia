package by.epam.alienpedia.command;

import by.epam.alienpedia.exception.CommandException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.dto.PublicationDto;
import by.epam.alienpedia.service.PublicationDtoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Designed to find of information about the publication.
 */
public class PublicationInfoCommand extends AbstractCommand {

    /**
     * Name of the request attribute.
     */
    private static final String PUBLICATION_DTO = "publicationDto";

    private static final String ALIEN_INFO_PAGE = "/WEB-INF/jsp/publication.jsp";
    private static final String PAGE_404 = "/WEB-INF/jsp/error404.jsp";

    /**
     * Process the GET request, finds publication info and generates a result of
     * processing in the form of {@link by.epam.alienpedia.command.CommandResult} object.
     *
     * @param request  An {@link HttpServletRequest} object that contains
     *                 client request
     * @param response An {@link HttpServletResponse} object that contains
     *                 the response the servlet sends to the client
     * @return Returns result of processing in the form of
     * {@link by.epam.alienpedia.command.CommandResult} object.
     * @throws CommandException Throws when
     *                          {@link by.epam.alienpedia.exception.ServiceException} Exception is caught.
     */
    @Override
    public CommandResult executeGet(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        CommandResult commandResult = new CommandResult(ALIEN_INFO_PAGE, false);

        long alienId = getPublicationIdFromRequest(request);

        PublicationDtoService publicationDtoService = new PublicationDtoService();
        Optional<PublicationDto> publicationDto;
        try {
            publicationDto = publicationDtoService.findById(alienId);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

        publicationDto.ifPresent(aDTOPubl -> request.setAttribute(PUBLICATION_DTO, aDTOPubl));

        return commandResult;
    }
}
