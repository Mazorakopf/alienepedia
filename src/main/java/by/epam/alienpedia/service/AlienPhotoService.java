package by.epam.alienpedia.service;

import by.epam.alienpedia.dao.AlienPhotoDao;
import by.epam.alienpedia.dao.impl.factory.DaoFactory;
import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.entity.AlienPhoto;

import java.util.List;

public class AlienPhotoService {

    public List<AlienPhoto> getPhotoByPublicationId(long id) throws ServiceException {
        try (AlienPhotoDao alienPhotoDao = DaoFactory.newAlienPhotoDao()) {

            return alienPhotoDao.findAlienPhotosByPublicationId(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public long addNewAlienPhoto(AlienPhoto alienPhoto) throws ServiceException {
        try (AlienPhotoDao alienPhotoDao = DaoFactory.newAlienPhotoDao()) {

            return alienPhotoDao.create(alienPhoto);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
