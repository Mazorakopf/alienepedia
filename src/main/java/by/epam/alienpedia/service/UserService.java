package by.epam.alienpedia.service;

import by.epam.alienpedia.dao.UserDao;
import by.epam.alienpedia.dao.impl.factory.DaoFactory;
import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.entity.User;

import java.util.List;
import java.util.Optional;

public class UserService {

    public Optional<User> login(String login, String password) throws ServiceException {

        try (UserDao userDao = DaoFactory.newUserDao()) {

            return userDao.findUserByLoginAndPassword(login, password);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public Optional<User> findById(long id) throws ServiceException {

        try (UserDao userDao = DaoFactory.newUserDao()) {

            return userDao.findById(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public List<User> findAll() throws ServiceException {

        try (UserDao userDao = DaoFactory.newUserDao()) {

            return userDao.findAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public boolean checkUsername(String username) throws ServiceException {

        try (UserDao userDao = DaoFactory.newUserDao()) {

            return userDao.findUserByLogin(username).isPresent();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public long registerUser(User user) throws ServiceException {
        try (UserDao userDao = DaoFactory.newUserDao()) {

            return userDao.create(user);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public void updateUser(User user) throws ServiceException {
        try (UserDao userDao = DaoFactory.newUserDao()) {

            userDao.update(user);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
