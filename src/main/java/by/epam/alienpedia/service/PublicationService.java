package by.epam.alienpedia.service;

import by.epam.alienpedia.dao.PublicationDao;
import by.epam.alienpedia.dao.TransactionManager;
import by.epam.alienpedia.dao.impl.factory.DaoFactory;
import by.epam.alienpedia.exception.DaoException;
import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.entity.Publication;

import java.util.List;
import java.util.Optional;

public class PublicationService {

    private TransactionManager transactionManager;

    public PublicationService() {
        transactionManager = new TransactionManager();
    }

    public List<Publication> findAllPublication() throws ServiceException {

        try (PublicationDao publicationDao = DaoFactory.newPublicationDao()) {

            return publicationDao.findAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public Optional<Publication> findById(long id) throws ServiceException {

        try (PublicationDao publicationDao = DaoFactory.newPublicationDao()) {

            return publicationDao.findById(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public List<Publication> findUserPublication(long userId) throws ServiceException {
        try (PublicationDao publicationDao = DaoFactory.newPublicationDao()) {

            return publicationDao.findUserPublications(userId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public List<Publication> findVerifiedPublication(int page, int publicationPerPage) throws ServiceException {
        try (PublicationDao publicationDao = DaoFactory.newPublicationDao()) {

            int position = (page == 1) ? 0 : (page - 1) * publicationPerPage;

            return publicationDao.findVerifiedPublications(position, publicationPerPage);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public long findPublicationPagesCount(int publicationPerPage) throws ServiceException {
        try (PublicationDao publicationDao = DaoFactory.newPublicationDao()) {

            long numberAllPublication = publicationDao.findNumberOfPublication();

            return (int) Math.ceil((double) numberAllPublication / publicationPerPage);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public long addNewPublication(Publication publication) throws ServiceException {
        try (PublicationDao publicationDao = DaoFactory.newPublicationDao()) {

            return publicationDao.create(publication);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public boolean isLikedPublication(Publication publication, long userId) throws ServiceException {
        try (PublicationDao publicationDao = DaoFactory.newPublicationDao()) {

            long publicationId = publication.getId();

            return publicationDao.isLikedPublication(publicationId, userId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    public void likedPublication(long publicationId, long userId) throws ServiceException {
        try (PublicationDao publicationDao = DaoFactory.newPublicationDao()) {
            transactionManager.beginTransaction(publicationDao);

            publicationDao.upPublicationRating(publicationId);
            publicationDao.likedPublication(publicationId, userId);

            transactionManager.commit();
        } catch (DaoException e) {
            transactionManager.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            transactionManager.endTransaction();
        }
    }

    public void unLikedPublication(long publicationId, long userId) throws ServiceException {
        try (PublicationDao publicationDao = DaoFactory.newPublicationDao()) {
            transactionManager.beginTransaction(publicationDao);

            publicationDao.downPublicationRating(publicationId);
            publicationDao.unLikedPublication(publicationId, userId);

            transactionManager.commit();
        } catch (DaoException e) {
            transactionManager.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            transactionManager.endTransaction();
        }
    }

    public long updatePublication(Publication publication) throws ServiceException {
        try (PublicationDao publicationDao = DaoFactory.newPublicationDao()) {

            return publicationDao.update(publication);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
