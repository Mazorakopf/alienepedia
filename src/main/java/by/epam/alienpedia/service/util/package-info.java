/**
 * A package for utils that work in a layer of services
 * and provide the basic logic of a web application.
 */
package by.epam.alienpedia.service.util;