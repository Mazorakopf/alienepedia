package by.epam.alienpedia.service.util;

import by.epam.alienpedia.exception.ServiceException;
import org.apache.commons.fileupload.FileItem;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Designed to work with file uploading process in singleton style.
 */
public final class FileUploader {

    /**
     * Names of the properties for initialization {@code FileUploader} fields.
     */
    private static final String FULL_SAVE_PATH = "fullSavePath";
    private static final String RELATIVE_SAVE_PATH = "relativeSavePath";

    private static final ReentrantLock LOCK = new ReentrantLock();
    private static final String PROPERTIES_FILE_FOR_IMAGE_UPLOAD = "image_upload.properties";

    /**
     * Flag to check the existence of the instance {@code FileUploader}.
     */
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
    private static FileUploader instance;

    private String fullSavePath;
    private String relativeSavePath;

    /**
     * Constructor with initialization of fields.
     */
    private FileUploader() {
        initializeData();
    }

    /**
     * Designed to get a single instance of a class with a double-checked
     * locking.
     *
     * @return {@code FileUploader} class instance.
     */
    public static FileUploader getInstance() {

        if (!instanceCreated.get()) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new FileUploader();
                    instanceCreated.set(true);
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    public String getFullSavePath() {
        return fullSavePath;
    }

    public String getRelativeSavePath() {
        return relativeSavePath;
    }

    /**
     * Upload file to destination directory.
     *
     * @param fileItem A {@link FileItem} object that contains file
     *                 that should be written.
     * @return A {@link String} object that contains written file name.
     * @throws ServiceException Signals that service exception of some sort
     *                          has occurred.
     */
    public String upload(final FileItem fileItem) throws ServiceException {

        String fileName = fileItem.getName();
        String pathname = fullSavePath + fileName;

        try {
            File uploadedFile = new File(pathname);
            fileItem.write(uploadedFile);
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return relativeSavePath + fileName;
    }

    /**
     * Designed for file uploading data initialization. Data is obtained from
     * properties file.
     */
    private void initializeData() {

        Class<? extends FileUploader> aClass = this.getClass();
        ClassLoader classLoader = aClass.getClassLoader();
        InputStream inputStream = classLoader
                .getResourceAsStream(PROPERTIES_FILE_FOR_IMAGE_UPLOAD);

        Properties property = new Properties();
        try {
            property.load(inputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException(
                    "File with properties not found! " + e.getMessage(), e);
        }
        fullSavePath = property.getProperty(FULL_SAVE_PATH);
        relativeSavePath = property.getProperty(RELATIVE_SAVE_PATH);
    }
}
