package by.epam.alienpedia.service;

import by.epam.alienpedia.exception.ServiceException;
import by.epam.alienpedia.model.dto.PublicationDto;
import by.epam.alienpedia.model.entity.AlienPhoto;
import by.epam.alienpedia.model.entity.Publication;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PublicationDtoService {

    public Optional<PublicationDto> findById(long id) throws ServiceException {
        PublicationService publicationService = new PublicationService();
        Optional<Publication> publication = publicationService.findById(id);

        AlienPhotoService alienPhotoService = new AlienPhotoService();
        List<AlienPhoto> alienPhotos = alienPhotoService.getPhotoByPublicationId(id);

        PublicationDto publicationDto = new PublicationDto(publication.get(), alienPhotos);
        return Optional.of(publicationDto);
    }

    public long findPublicationPagesCount(int publicationPerPage) throws ServiceException {
        return new PublicationService().findPublicationPagesCount(publicationPerPage);
    }

    public List<PublicationDto> findVerifiedPublication(int page, int publicationPerPage) throws ServiceException {
        PublicationService publicationService = new PublicationService();
        List<Publication> publications = publicationService.findVerifiedPublication(page, publicationPerPage);

        List<PublicationDto> publicationDtos = new ArrayList<>();

        AlienPhotoService alienPhotoService = new AlienPhotoService();
        findAlienPhotos(publications, alienPhotoService, publicationDtos);

        return publicationDtos;
    }

    public boolean addNewPublication(PublicationDto publicationDto) throws ServiceException {
        PublicationService publicationService = new PublicationService();
        AlienPhotoService alienPhotoService = new AlienPhotoService();

        Publication publication = publicationDto.getPublication();

        long publicationId = publicationService.addNewPublication(publication);
        List<AlienPhoto> alienPhotos = publicationDto.getPhotos();
        for (final AlienPhoto photo : alienPhotos) {
            photo.setPublicationId(publicationId);
            alienPhotoService.addNewAlienPhoto(photo);
        }

        return true;
    }

    public void findLikedPublications(List<PublicationDto> publicationDtos, long userId) throws ServiceException {
        PublicationService publicationService = new PublicationService();
        for (final PublicationDto publicationDto : publicationDtos) {
            Publication publication = publicationDto.getPublication();
            boolean liked = publicationService.isLikedPublication(publication, userId);
            publicationDto.setLiked(liked);
        }
    }

    private void findAlienPhotos(List<Publication> publications,
                                 AlienPhotoService alienPhotoService,
                                 List<PublicationDto> publicationDtos) throws ServiceException {
        for (Publication publication : publications) {
            long publicationId = publication.getId();
            List<AlienPhoto> publicationPhotos = alienPhotoService.getPhotoByPublicationId(publicationId);

            PublicationDto publicationDto = new PublicationDto(publication, publicationPhotos);
            publicationDtos.add(publicationDto);
        }
    }
}

