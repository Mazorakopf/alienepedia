<%@ page contentType="text/html;charset=UTF-8" language="java"  isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" tagdir="/WEB-INF/tags" %>

<fmt:setLocale value="${sessionScope.language}" />
<fmt:setBundle basename="local" var="local"/>

<html lang="${sessionScope.language}>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="stylesheet" type="text/css" href="css/header.css">
    <title>Registration Page</title>
</head>
<u:header/>
<body>
<div class="registration-page">
    <div class="form">
        <c:if test = "${not empty errorMessage}">
            <div class="alert alert-danger text-center mt-1">
                <strong>${errorMessage}</strong>
                <c:remove var = "errorMessage" scope = "session" />
            </div>
        </c:if>
        <form class="register-form" id="registerForm" action="controller" method="post">
            <input type="hidden" name="command" value="registration" />
            <input type="text" name="firstName" placeholder="First name" />
            <input type="text" name="lastName" placeholder="Last name" />
            <input type="text" name="username" placeholder="Username" required/>
            <input type="password" id="password" name="password" placeholder="Password" required/>
            <input type="password" name="rePassword" placeholder="Repeat password"/>
            <input type="email" name="email" placeholder="email address"/>
            <button type="submit">create</button>
        </form>
        <p class="message">
            Already registered? <a href="login">Sign In</a>
        </p>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<script>
    $("#registerForm").validate({
        rules: {
            password: {
                required: true,
                minlength: 4
            } ,

            rePassword: {
                equalTo: "#password",
                minlength: 4
            }
        },
        messages:{
            password: {
                required:"The password is required."
            },
            rePassword: {
                equalTo:"Passwords do not match."
            }
        }
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>
