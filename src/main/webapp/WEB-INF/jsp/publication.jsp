<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" tagdir="/WEB-INF/tags" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="local" var="local"/>

<html lang="${sessionScope.language}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/alienInfo.css">
    <title>${requestScope.publicationDto.publication.name}</title>
</head>
<u:header/>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="background-color: white; height: 100%; margin-top: 100px">
            <div class="row" style="background-color: #cccccc">
                <div class="col">
                    <img src="img/Wp-wotmfa.png" alt="" width="24"
                         height="24">
                    <span style="color:black;">
                        <b>${requestScope.publicationDto.publication.name}</b>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    ${requestScope.publicationDto.publication.about}
                </div>
                <div class="col-md-4">
                    <aside class="portable-infobox pi-background pi-theme-wikia pi-layout-default">
                        <h2 class="pi-item pi-item-spacing pi-title" style="width: 250px;">
                            <span style="color: #ffffff;">${requestScope.publicationDto.publication.name}</span></h2>

                        <figure class="pi-item pi-image">
                            <img src="${requestScope.publicationDto.photos.get(0).url}" alt="" width="250" height="274">
                        </figure>

                        <div class="pi-item pi-data pi-item-spacing pi-border-color">
                            <div class="pi-data-value pi-font"><b>Другие прозвища:</b>
                                <div class="group-layout">${requestScope.publicationDto.publication.nicknames}</div>
                            </div>
                        </div>
                        <section class="pi-item pi-group pi-border-color">
                            <h2 class="pi-item pi-header pi-secondary-font pi-item-spacing pi-secondary-background">
                                Личность</h2>
                            <div class="pi-item pi-data pi-item-spacing pi-border-color">
                                <h3 class="pi-data-label pi-secondary-font" style="margin-right: 60px;">Позиция:</h3>
                                <div class="pi-data-value pi-font">${requestScope.publicationDto.publication.position}</div>
                            </div>
                        </section>
                        <section class="pi-item pi-group pi-border-color">
                            <h2 class="pi-item pi-header pi-secondary-font pi-item-spacing pi-secondary-background">
                                Происхождение</h2>

                            <div class="pi-item pi-data pi-item-spacing pi-border-color">
                                <h3 class="pi-data-label pi-secondary-font">Вселенная:</h3>
                                <div class="pi-data-value pi-font"><p>
                                    <a>${requestScope.publicationDto.publication.universe}</a></p></div>
                            </div>

                            <div class="pi-item pi-data pi-item-spacing pi-border-color">
                                <div class="pi-data-value pi-font"><b>Место рождения:</b>
                                    <div class="group-layout"><a
                                            class="mw-redirect">${requestScope.publicationDto.publication.origin}</a>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </aside>
                </div>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>
