<fmt:message bundle="${local}" key="main.article.table.link.edit" var="edit"/>
<fmt:message bundle="${local}" key="main.article.table.info.read" var="read"/>
<style>
    .button-like {
        background-image: url("img/liked.svg");
        border: none;
        height: 24px;
        width: 24px;
        right: 0;
        position: absolute;
    }

    .button-unlike {
        background-image: url("img/noLiked.svg");
        border: none;
        height: 24px;
        width: 24px;
        right: 0;
        position: absolute;
    }

    .button-blocked {
        outline: none;
        border: none;
        margin-left: 80%;
        margin-top: 0.5%;
    }
</style>
<article class="WikiaMainCintent" id="WikiaMainCintent">
    <div class="WikiaMainCintentContainer" id="WikiaMainCintentContainer">
        <div class="WikiaArticle" id="WikiaArticle">
            <table style="width: 100%; vertical-align:top; background-color:white; table-layout: fixed"
                   cellspacing="0" cellpadding="2">
                <tr>
                    <td>
                        <table style="width: 100%; vertical-align: top; background-color:white; table-layout: fixed"
                               cellspacing="3" cellpadding="2">
                            <tbody>
                            <c:forEach var="publicationDto" items="${requestScope.publications}">
                                <tr>
                                    <th class="nounderlinelink" style="text-align: left; padding: 0px">
                                        <div style="width: 100%; background-color: #D8E9FC; display:table; border-radius:5px; border:1px solid #D8E9FC;">
                                            <table style="width: 100%;">
                                                <tbody>
                                                <tr>
                                                    <th style="text-align: left;">
                                                        <img src="img/Wp-wotmfa.png" alt="" width="24"
                                                             height="24">
                                                        <span style="color:black;">
                                                        <b>${publicationDto.publication.name}</b>
                                                    </span>
                                                    </th>
                                                    <c:if test="${sessionScope.role.value eq 'user'}">
                                                        <td style="font-size: 0.8em;">
                                                            <c:choose>
                                                                <c:when test="${publicationDto.liked}">
                                                                    <form class="like-form"
                                                                          action="controller?command=unlike&alienId=${publicationDto.publication.id}"
                                                                          method="post"
                                                                          style="margin-right: 1%; height: 26px">
                                                                        <button onclick="className = (className.equalTo('button-like')) ? 'button-unlike' : 'button-like'"
                                                                                class="button-like"></button>
                                                                    </form>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <form class="unlike-form"
                                                                          action="controller?command=like&alienId=${publicationDto.publication.id}"
                                                                          method="post"
                                                                          style="margin-right: 1%; height: 26px">
                                                                        <button onclick="className = (className.equalTo('button-unlike')) ? 'button-like' : 'button-unlike'"
                                                                                class="button-unlike"></button>
                                                                    </form>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                    </c:if>
                                                    <c:if test="${sessionScope.role.value eq 'admin'}">
                                                        <td style="font-size: 0.8em;">
                                                            <form class="like-form"
                                                                  action="controller?command=blockedPublication&alienId=${publicationDto.publication.id}"
                                                                  method="post"
                                                                  style="margin-right: 1%; height: 26px">
                                                                <button class="button-blocked">Blocked</button>
                                                            </form>
                                                        </td>
                                                    </c:if>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="showcase">
                                            <table style="width: 100%;" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                <tr>
                                                    <td style="text-align: left">
                                                        <div class="showcase">
                                                            <div class="floatleft">
                                                                <img src="${publicationDto.photos.get(0).url}" alt=""
                                                                     class="info-img">
                                                                <b><span class="about">
                                                                        ${publicationDto.publication.about}
                                                                </span>
                                                                    <a href="controller?command=alienInfo&alienId=${publicationDto.publication.id}">(${read}) </a></b>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
            <c:url value="/controller?command=main&page=##" var="url"/>
            <paginator:display maxLinks="2" currentPage="${requestScope.page}" totalPages="${requestScope.pageCount}"
                               url="${url}"/>
        </div>
    </div>
</article>