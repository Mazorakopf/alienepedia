<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" tagdir="/WEB-INF/tags" %>

<html lang="${sessionScope.language}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <link rel="stylesheet" type="text/css" href="css/header.css">
</head>
<u:header/>
<body>
<div class="add-publication-page" style="margin-top: 100px">
    <div class="form" style="max-width: 700px">
        <form class="add-publication-form" action="controller?command=addNewPublication" method="post"
              enctype="multipart/form-data">
            <input class="form-control form-control-lg" type="text" name="alienName" placeholder="Alien name">
            <input class="form-control form-control-lg" type="text" name="alienNicknames" placeholder="Alien nicknames">
            <input class="form-control form-control-lg" type="text" name="alienUniverse" placeholder="Alien universe">
            <input class="form-control form-control-lg" type="text" name="alienOrigin" placeholder="Alien origin">
            <select name="alienPosition" required>
                <option disabled>Position</option>
                <option value="GOOD">GOOD</option>
                <option value="EVIL">EVIL</option>
                <option value="NEUTRALITY">NEUTRALITY</option>
            </select>
            <div class="form-group">
                <label for="comment">About alien</label>
                <textarea class="form-control" name="alienAbout" id="comment"></textarea>
            </div>
            <input type="file" name="alienPhoto" class="form-control-file border">
            <button type="submit" style="width: 40%">Add new publication</button>
        </form>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</html>
