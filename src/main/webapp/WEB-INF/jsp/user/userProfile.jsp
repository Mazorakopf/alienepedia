<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" tagdir="/WEB-INF/tags" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="local" var="local"/>

<fmt:message bundle="${local}" key="profile.head.title" var="title"/>
<fmt:message bundle="${local}" key="profile.nav.link.pubication" var="publications"/>
<fmt:message bundle="${local}" key="profile.nav.link.edit" var="edit"/>
<fmt:message bundle="${local}" key="profile.info.username" var="username"/>
<fmt:message bundle="${local}" key="profile.info.first.name" var="firstName"/>
<fmt:message bundle="${local}" key="profile.info.last.name" var="lastName"/>
<fmt:message bundle="${local}" key="profile.info.email" var="email"/>
<fmt:message bundle="${local}" key="profile.publication.info.name" var="publicationName"/>
<fmt:message bundle="${local}" key="profile.publication.info.date" var="date"/>
<fmt:message bundle="${local}" key="profile.publication.info.verified" var="verified"/>
<fmt:message bundle="${local}" key="profile.publication.info.rating" var="rating"/>
<fmt:message bundle="${local}" key="profile.info.edit.password" var="password"/>
<fmt:message bundle="${local}" key="profile.info.edit.confirm" var="confirm"/>

<html lang="${sessionScope.language}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <meta charset="UTF-8">
    <title>${title}</title>
</head>
<style>
    body {
        background-color: gray
    }

    #main {
        background-color: #f2f2f2;
        padding: 20px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        border-bottom: 4px solid #ddd;
    }

    #profileContainer {
        position: absolute;
        top: 100px;
        margin-left: 8%;
    }

    .tab-content button {
        font-family: "Roboto", sans-serif;
        text-transform: uppercase;
        outline: 0;
        background: #007bff;
        width: 20%;
        border: 0;
        padding: 15px;
        color: #FFFFFF;
        font-size: 14px;
        -webkit-transition: all 0.3 ease;
        transition: all 0.3 ease;
        cursor: pointer;
    }

    .tab-content button:hover, .tab-content button:active, .tab-content button:focus {
        background: #0051A6;
    }

    input {
        font-family: "Roboto", sans-serif;
        outline: 0;
        background: #f2f2f2;
        width: 100%;
        border: 0;
        margin: 0 0 15px;
        padding: 15px;
        box-sizing: border-box;
        font-size: 14px;
    }
</style>
<u:header/>
<body>

<jsp:useBean id="user" type="by.epam.alienpedia.model.entity.User" scope="request"/>

<div class="container" id="profileContainer">
    <div id="main">
        <div class="row" id="real-estates-detail">
            <div class="col-lg-8 col-md-8 col-xs-12">
                <div class="panel">
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#ProfileInfo">${title}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#PublicationsList">${publications}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#Edit">${edit}</a>
                            </li>
                        </ul>
                        <div class="tab-content border mb-3">
                            <div id="ProfileInfo" class="container tab-pane active"><br>
                                <table class="table table-th-block">
                                    <tbody>
                                    <tr>
                                        <td class="active">${username}:</td>
                                        <td>${user.userName}</td>
                                    </tr>
                                    <tr>
                                        <td class="active">${firstName}:</td>
                                        <td>${user.firstName}</td>
                                    </tr>
                                    <tr>
                                        <td class="active">${lastName}:</td>
                                        <td>${user.lastName}</td>
                                    </tr>
                                    <tr>
                                        <td class="active">${email}:</td>
                                        <td>${user.email}</td>
                                    </tr>
                                    <%--<tr><td class="active">Рейтинг пользователя:</td><td><i class="fa fa-star" style="color:red"></i> <i class="fa fa-star" style="color:red"></i> <i class="fa fa-star" style="color:red"></i> <i class="fa fa-star" style="color:red"></i> 4/5</td></tr>--%>
                                    </tbody>
                                </table>
                            </div>
                            <div id="PublicationsList" class="container tab-pane fade"><br>
                                <table class="table">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>${publicationName}</th>
                                        <th>${date}</th>
                                        <th>${verified}</th>
                                        <th>${rating}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${requestScope.publications}" var="publication">
                                        <tr>
                                            <td><a href="controller?command=alienInfo&alienId=${publication.id}">
                                                    ${publication.name}
                                            </a></td>
                                            <td>${publication.dateOfPublication}</td>
                                            <c:choose>
                                                <c:when test="${publication.verified eq true}">
                                                    <td><input type="checkbox" checked disabled/></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td><input type="checkbox" disabled/></td>
                                                </c:otherwise>
                                            </c:choose>
                                            <td>${publication.rating}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <button onclick="location.href = 'newPublication'">Add new</button>
                            </div>
                            <div id="Edit" class="container tab-pane fade">
                                <c:if test="${not empty errorMessage}">
                                    <div class="alert alert-danger text-center mt-1">
                                        <strong>${errorMessage}</strong>
                                        <c:remove var="errorMessage" scope="session"/>
                                    </div>
                                </c:if>
                                <form class="edit-profile-form" id="editForm" action="controller" method="post">
                                    <input type="hidden" name="command" value="editProfile"/>
                                    <table class="table table-th-block">
                                        <tbody>
                                        <tr>
                                            <td class="active">${firstName}:</td>
                                            <td><input type="text" name="firstName" placeholder="${user.firstName}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="active">${lastName}:</td>
                                            <td><input type="text" name="lastName" placeholder="${user.lastName}"/></td>
                                        </tr>
                                        <tr>
                                            <td class="active">${email}:</td>
                                            <td><input type="email" name="email" placeholder="${user.email}"/></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <button>Edit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
        });
        $('.nav-tabs a').on('shown.bs.tab', function (event) {
            var x = $(event.target).text();         // active tab
            var y = $(event.relatedTarget).text();  // previous tab
            $(".act span").text(x);
            $(".prev span").text(y);
        });
    });
</script>
</body>
</html>