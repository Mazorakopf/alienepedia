<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="u" tagdir="/WEB-INF/tags" %>

<html lang="${sessionScope.language}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <meta charset="UTF-8">
    <title>${title}</title>
</head>
<style>
    body {
        background-color: gray
    }

    #main {
        background-color: #f2f2f2;
        padding: 20px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
        border-bottom: 4px solid #ddd;
    }

    #managementContainer {
        position: absolute;
        top: 100px;
        margin-left: 8%;
    }

    .tab-content button {
        font-family: "Roboto", sans-serif;
        text-transform: uppercase;
        outline: 0;
        background: #007bff;
        width: 20%;
        border: 0;
        padding: 15px;
        color: #FFFFFF;
        font-size: 14px;
        -webkit-transition: all 0.3 ease;
        transition: all 0.3 ease;
        cursor: pointer;
    }

    .tab-content button:hover, .tab-content button:active, .tab-content button:focus {
        background: #0051A6;
    }

    input {
        font-family: "Roboto", sans-serif;
        outline: 0;
        background: #f2f2f2;
        width: 100%;
        border: 0;
        margin: 0 0 15px;
        padding: 15px;
        box-sizing: border-box;
        font-size: 14px;
    }
</style>
<u:header/>
<body>
<div class="container" id="managementContainer">
    <div id="main">
        <div class="row" id="real-estates-detail">
            <div class="col-lg-8 col-md-8 col-xs-12">
                <div class="panel">
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#Publications">Publication</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#Users">Users</a>
                            </li>
                        </ul>
                        <div class="tab-content border mb-3">
                            <div id="Publications" class="container tab-pane active"><br>
                                <table class="table">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>Username</th>
                                        <th>Publication Name</th>
                                        <th>Date</th>
                                        <th>verified</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${publications}" var="publication">
                                        <tr>
                                            <c:forEach items="${users}" var="user">
                                                <c:if test="${user.id eq publication.ownerId}">
                                                    <td>${user.userName}</td>
                                                </c:if>
                                            </c:forEach>
                                            <td><a href="controller?command=alienInfo&alienId=${publication.id}">
                                                    ${publication.name}
                                            </a></td>
                                            <td>${publication.dateOfPublication}</td>
                                            <form class="blocked/verified-publication-form" action="controller"
                                                  method="post">
                                                <input type="hidden" name="alienId" value="${publication.id}">
                                                <c:choose>
                                                    <c:when test="${publication.verified eq true}">
                                                        <input type="hidden" name="command" value="blockedPublication">
                                                        <td>
                                                            <button type="submit" style="width: 100px">Blocked</button>
                                                        </td>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <input type="hidden" name="command" value="verifiedPublication">
                                                        <td>
                                                            <button type="submit" style="width: 100px">Verified</button>
                                                        </td>
                                                    </c:otherwise>
                                                </c:choose>
                                            </form>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div id="Users" class="container tab-pane fade"><br>
                                <table class="table">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>Username</th>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>email</th>
                                        <th>Publications</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${users}" var="user">
                                        <tr>
                                            <td>${user.userName}</td>
                                            <td>${user.firstName}</td>
                                            <td>${user.lastName}</td>
                                            <td>${user.email}</td>
                                            <td>
                                                <c:forEach items="${publications}" var="publication">
                                                    <c:if test="${user.id eq publication.ownerId}">
                                                        <a href="controller?command=alienInfo&alienId=${publication.id}">
                                                                ${publication.name}</a>
                                                    </c:if>
                                                </c:forEach>
                                            </td>
                                            <form class="banned-users-form" action="controller" method="post">
                                                <input type="hidden" name="userId" value="${user.id}">
                                                <c:choose>
                                                    <c:when test="${user.banned eq false}">
                                                        <input type="hidden" name="command" value="banUser">
                                                        <td>
                                                            <button type="submit" style="width: 100px">Ban</button>
                                                        </td>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <input type="hidden" name="command" value="unBanUser">
                                                        <td>
                                                            <button type="submit" style="width: 100px">UnBan</button>
                                                        </td>
                                                    </c:otherwise>
                                                </c:choose>
                                            </form>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
        });
        $('.nav-tabs a').on('shown.bs.tab', function (event) {
            var x = $(event.target).text();         // active tab
            var y = $(event.relatedTarget).text();  // previous tab
            $(".act span").text(x);
            $(".prev span").text(y);
        });
    });
</script>
</body>
</html>
