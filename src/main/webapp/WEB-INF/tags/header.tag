<%@ tag language="java" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="local" var="local"/>

<fmt:message bundle="${local}" key="main.section.header.home" var="home"/>
<fmt:message bundle="${local}" key="header.nav.item.profile" var="profile"/>
<fmt:message bundle="${local}" key="header.nav.item.management" var="management"/>
<fmt:message bundle="${local}" key="header.nav.dropdown.item.login" var="login"/>
<fmt:message bundle="${local}" key="header.nav.dropdown.item.registration" var="registration"/>
<fmt:message bundle="${local}" key="header.nav.dropdown.item.logout" var="logout"/>

<header>
    <nav class="navbar navbar-expand-lg bg-dark fixed-top scrolling-navbar">
        <div class="container">
            <a class="navbar-brand" href="controller?command=main">
                <img src="img/Alien.svg" width="35" height="35" class="d-inline-block align-top" alt="">
                ALIENEPEDIA
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicNav"
                    aria-controls="basicNav" aria-expanded="false" aria-label="Toggle Navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="basicNav">
                <ul class="navbar-nav mr-auto smooth-scroll">
                    <li class="nav-item active">
                        <a class="nav-link waves-effect waves-light" href="controller?command=main">${home}</a>
                    </li>
                    <li class="nav-item">
                        <c:if test="${not empty userId}">
                            <c:choose>
                                <c:when test="${role eq 'USER'}">
                                    <a class="nav-link waves-effect waves-light"
                                       href="controller?command=profile">${profile}</a>
                                </c:when>
                                <c:when test="${role eq 'ADMIN'}">
                                    <a class="nav-link"
                                       href="controller?command=management">${management}</a>
                                </c:when>
                            </c:choose>
                        </c:if>
                    </li>
                </ul>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-primary">
                    <img src="img/user.svg" width="35" height="35" class="d-inline-block align-top" alt=""
                         style="padding-bottom: 5px;padding-right: 10px;">
                </button>
                <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split"
                        data-toggle="dropdown">
                </button>
                <div class="dropdown-menu">
                    <c:choose>
                        <c:when test="${empty userId}">
                            <a class="dropdown-item" href="login">${login}</a>
                            <a class="dropdown-item" href="registration">${registration}</a>
                        </c:when>
                        <c:when test="${not empty userId}">
                            <a class="dropdown-item" href="controller?command=logout">${logout}</a>
                        </c:when>
                    </c:choose>
                </div>
            </div>
            <div>
                <button onclick="location.href = 'controller?command=language&language=${sessionScope.nextLanguage}'"
                        class="btn btn-primary" style="float:right">${sessionScope.nextLanguage}</button>
            </div>
        </div>
    </nav>
</header>